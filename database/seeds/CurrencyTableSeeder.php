<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$currencies = [
    		[
    			'id' => 'PEN',
    			'name' => 'Sol peruano',
    			'symbol' => 'S/.',
    			'iso_country' => 'PE',
    		],
    		[
    			'id' => 'BOB',
    			'name' => 'Boliviano',
    			'symbol' => 'Bs.',
    			'iso_country' => 'BO',
    		],
    		[
    			'id' => 'USD',
    			'name' => 'Dólar estadounidense',
    			'symbol' => '$',
    			'iso_country' => 'US',
    		]
    	];

    	DB::table('currencies')->insert($currencies);
    }
}
