<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $date = Carbon\Carbon::create()->toDateTimeString();

        $userType = ['name' => 'User', 'created_at' => $date, 'updated_at' => $date];

        $adminType = ['name' => 'Owner', 'created_at' => $date, 'updated_at' => $date];

        $db = DB::table('user_types')->insert($userType);

        $db = DB::table('user_types')->insert($adminType);

        Model::reguard();
    }
}
