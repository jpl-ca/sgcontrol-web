<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_order_items', function (Blueprint $table) {

            //Setting columns
            $table->increments('id');
            $table->string('name');
            $table->decimal('price');
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('product_order_id')->unsigned()->nullable();
            $table->decimal('quantity')->nullable();
            $table->text('description')->nullable();

            // cache fields
            $table->text('cache_description');
            $table->string('cache_category');
            $table->string('cache_brand');
            $table->string('cache_model');
            
            $table->timestamps();

            //Setting foreign keys
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('set null');
            
            $table->foreign('product_order_id')
                ->references('id')
                ->on('product_orders')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_order_items');
    }
}
