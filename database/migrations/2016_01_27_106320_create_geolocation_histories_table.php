<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeolocationHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geolocation_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('trackable_id');
            $table->decimal('lat', 17, 15);
            $table->decimal('lng', 18, 15);
            $table->unsignedInteger('task_id')->nullable();
            $table->unsignedInteger('recordable_data_id')->nullable();
            $table->timestamps();

            $table->foreign('trackable_id')
                ->references('id')
                ->on('trackables')
                ->onDelete('cascade');

            $table->foreign('task_id')
                ->references('id')
                ->on('tasks')
                ->onDelete('set null');

            $table->foreign('recordable_data_id')
                ->references('id')
                ->on('recordable_datas')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('geolocation_histories');
    }
}
