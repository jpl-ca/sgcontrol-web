<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrackableIntoProductOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_orders', function (Blueprint $table) {
            $table->integer('trackable_id')->nullable();
            $table->string('trackable_type')->nullable();
            $table->string('trackable_fullname')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_orders', function (Blueprint $table) {
            $table->dropColumn('trackable_id');
            $table->dropColumn('trackable_type');
            $table->dropColumn('trackable_fullname');
        });
    }
}
