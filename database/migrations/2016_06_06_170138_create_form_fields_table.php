<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_fields', function (Blueprint $table) {

            // Setting fields
            $table->increments('id');
            $table->string('machine_name');
            $table->string('label');
            $table->string('type');
            $table->string('description')->nullable();
            $table->integer('form_template_id')->unsigned();
            
            // Timestamps
            $table->timestamps();

            // Setting foreign keys
            $table->foreign('form_template_id')
                  ->references('id')
                  ->on('form_templates')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('form_fields');
    }
}
