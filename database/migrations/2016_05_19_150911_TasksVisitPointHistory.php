<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TasksVisitPointHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks_visit_point_histories', function (Blueprint $table) {

            //Setting fields
            $table->increments('id');
            $table->integer('tasks_visit_point_id');
            $table->decimal('lat', 17, 15);
            $table->decimal('lng', 17, 15);
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('reference')->nullable();
            $table->unsignedInteger('task_id');
            $table->unsignedInteger('visit_state_id')->default(1);
            $table->unsignedInteger('marker_id')->nullable();

            $table->integer('visit_point_id_parent')
                ->unsigned()
                ->nullable();

            $table->datetime('datetime')
                ->nullable();

            $table->text('comment_of_rescheduling')
                ->nullable();

            $table->timestamps();

            // Add references
            $table->foreign('task_id')
                ->references('id')
                ->on('tasks')
                ->onDelete('cascade');

            $table->foreign('visit_state_id')
                ->references('id')
                ->on('visit_states')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('marker_id')
                ->references('id')
                ->on('markers')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks_visit_point_histories');
    }
}
