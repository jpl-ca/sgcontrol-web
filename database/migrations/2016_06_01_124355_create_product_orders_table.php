<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_orders', function (Blueprint $table) {

            $table->increments('id');

            $table->text('description');
            $table->string('organization_id')->nullable();
            $table->integer('tasks_visit_point_id')->unsigned()->nullable();
            $table->timestamps();

            //Setting foreign keys
            $table->foreign('organization_id')
                ->references('id')
                ->on('organizations')
                ->onDelete('set null');

            $table->foreign('tasks_visit_point_id')
                ->references('id')
                ->on('tasks_visit_points')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_orders');
    }
}
