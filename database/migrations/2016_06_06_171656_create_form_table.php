<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {

            // Setting fields
            $table->increments('id');
            $table->string('title');
            $table->integer('form_template_id')->unsigned();
            $table->integer('tasks_visit_point_id')->unsigned();
            $table->string('description')->nullable();

            // Setting cache fields
            $table->string('organization_id')->nullable();
            $table->integer('trackable_id')->unsigned()->nullable();

            // Timestamps
            $table->timestamps();

            // Setting foreigns
            $table->foreign('form_template_id')
                  ->references('id')
                  ->on('form_templates')
                  ->onDelete('cascade');

            $table->foreign('tasks_visit_point_id')
                  ->references('id')
                  ->on('tasks_visit_points')
                  ->onDelete('cascade');

            $table->foreign('trackable_id')
                  ->references('id')
                  ->on('trackables')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forms');
    }
}
