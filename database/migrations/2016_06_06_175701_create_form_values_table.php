<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_values', function (Blueprint $table) {
            // Setting fields
            $table->increments('id');
            $table->string('value');
            $table->integer('form_id')->unsigned();
            $table->integer('form_fields_id')->unsigned();

            $table->timestamps();

            // Cache fields
            $table->integer('tasks_visit_point_id')
                  ->unsigned()
                  ->nullable();

            $table->integer('form_template_id')
                  ->unsigned();

            $table->string('organization_id')
                  ->nullable();

            $table->integer('tasks_id')
                  ->nullable();

            $table->string('machine_name')
                  ->nullable();

            $table->string('label')
                  ->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('form_values');
    }
}
