<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVisitAndReferenceToTasksVisitPoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks_visit_points', function (Blueprint $table) {
            $table->integer('visit_point_id_parent')
                ->unsigned()
                ->nullable();

            $table->datetime('datetime')
                ->nullable();

            $table->text('comment_of_rescheduling');

            $table->foreign('visit_point_id_parent')
                ->references('id')
                ->on('tasks_visit_points')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks_visit_points', function (Blueprint $table) {
            $table->dropForeign('tasks_visit_points_visit_point_id_parent_foreign');
            $table->dropColumn('visit_point_id_parent');
            $table->dropColumn('datetime');
            $table->dropColumn('comment_of_rescheduling');
        });
    }
}
