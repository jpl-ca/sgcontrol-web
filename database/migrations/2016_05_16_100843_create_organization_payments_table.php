<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_payments', function (Blueprint $table) {

            $table->increments('id');

            $table->enum('type', ['manual', 'automatic'])->default('manual');
            $table->decimal('amount', 10, 2)->default(0);
            $table->date('date')->nullable();
            $table->text('additional_information')->nullable();
            $table->date('subscription_start_date');
            $table->date('subscription_end_date');
            $table->string('image_path')->nullable();
            $table->string('organization_id');

            $table->timestamps();

            //references
            $table->foreign('organization_id')
                ->references('id')
                ->on('organizations')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organization_payments');
    }
}
