<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trackable_id');
            $table->string('trackable_type');
            $table->string('api_token', 60)->unique()->nullable();
            $table->string('notification_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trackables');
    }
}
