<style>
	body {
		margin: 0px;
	}

	.main-container {
		
	}

	.header {
		
	}

	.header img {
		width: 25%;
	}
	
	.message-body {
		
	}

	.welcome-text {
		
	}

	.welcome-detail {

	}

	.welcome-detail .bold{
		
	}

	.thank-you-for-your-registration {
		
	}
	
	.container-btn-activation-link {
		
	}

	.btn-activation-link
	{
		
	}

	.feature-list {
		
	}

	.alternative-link {
		display: inline
	}

	.footer {
		
	}

	.footer img {
		
	}
</style>

<div class='main-container' style="color: #494949; font-family: Verdana;">
	<div class='header' style="background-color: #5bbb5f; padding-bottom: 15px; padding-top: 15px; padding-left: 15px;">
		<img src="{{$logoUrl}}" alt="Logo" style="width: 100px;">
	</div>

	<div class='message-body' style="padding: 20px;">
		<h1 class='welcome-text' style="color: #5bbb5f; font-weight: bold;">Bienvenido</h1>
		<span class='welcome-detail'>
			¡Ya estás a solo un paso de ser parte de <span class='bold' style="font-weight: bold;">SGVentas</span> !
		</span>

		<p class='thank-you-for-your-registration'>
			Gracias por registrarte en SGVentas. Trabajamos para ofrecerte todo lo que necesitas para tener un trabajo automatizado y mejor organizado.
		</p>

		<ul class='feature-list'>
			<li>Registra tus clientes, sin límites y para siempre.</li>
			<li>Programa la visita de tus clientes ilimitadamente.</li>
			<li>Luego del primer mes paga sólo US $4.99 por agente.</li>
		</ul>
		<div>
			Tus datos de acceso son:
			<ul>
				<li>Usuario: {{ $user }}</li>
				<li>Clave: {{ $password }}</li>
			</ul>
		</div>

		<div class='container-btn-activation-link' style="text-align: center; padding-top: 30px; padding-bottom: 30px;">
			<a href="{{$link}}" class='btn-activation-link' style="margin: 0px auto; background: #5bbb5f; color: white; font-weight: bold; padding: 15px; text-decoration: none;">
				Ingresar a mi cuenta ahora
			</a>
		</div>		
	</div>
	<div class="footer" style="text-align: center; font-size: 11px;">
		<span> Desarrollado por SGTEL. Todos los derechos reservados </span>
		<img src="http://sgtel.pe/dist/img2/logo.png" alt="" width="50" style="width: 50px;">
	</div>
</div>