@if(Session::has('success'))
    <div class="hidden toastr" data-type="success" data-message="{{ Session::get('success') }}"></div>
@elseif(Session::has('info'))
    <div class="hidden toastr" data-type="info" data-message="{{ Session::get('info') }}"></div>
@elseif(Session::has('warning'))
    <div class="hidden toastr" data-type="warning" data-message="{{ Session::get('warning') }}"></div>
@elseif(Session::has('danger'))
    <div class="hidden toastr" data-type="danger" data-message="{{ Session::get('danger') }}"></div>
@endif