<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/admin') }}">
                <img class="logo-sg" src="{{ url('/assets/irisgps/img-web/logo.png') }}" alt="logo">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @if (auth('admin')->check())
                    <li><a id="item" href="{{ action('Web\Admin\HomeController@index') }}">Principal</a></li>
                    <li><a id="organization-nav-item" href="{{ action('Web\Admin\OrganizationController@index') }}">Organizaciones</a></li>
                    <li><a id="suggestions-nav-item" href="{{ action('Web\Admin\SuggestionController@index') }}">Sugerencias</a></li>
                    <li>
                        <a id="report-nav-item" 
                         href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                         Reportes <span class="caret"></span>
                         </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('admin/reports/a') }}"> Agentes por Organizaciones</a></li>
                            <li><a href="{{ url('admin/reports/b') }}"> Visitas</a></li>
                        </ul>
                    </li>
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (auth('admin')->check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ auth('admin')->user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/admin/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Salir</a></li>
                        </ul>
                    </li>
                    
                @endif
            </ul>
        </div>
    </div>
</nav>