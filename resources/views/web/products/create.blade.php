@extends('layouts.web')

@section('body-tag')
    ng-app="app"
@endsection

@section('content')
<style>
	footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container-fluid"  ng-controller="ProductController">
        <div class="container  margin-top-40">
            <div class="col-md-8 col-md-offset-2 card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Nuevo Producto
                <a href="{{ action('Web\ProductController@index') }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a>
                </h2>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="cabecera">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                        			<form id="registerForm" name="registerForm" class="ng-pristine ng-valid" ng-submit="addProduct()">
			                            <div class="form-group">
										    <label for="Nombre" class="control-label col-md-3">Nombre:</label>
										    <div class="col-md-9">
										        <input class="form-control" required autocomplete="off" ng-model="product.name" type="text">
										    </div>
										</div>
										<div class="form-group">
										    <label for="Descripción" class="control-label col-md-3">Descripción:</label>
										    <div class="col-md-9">
										        <textarea class="form-control" required autocomplete="off" ng-model="product.description">
										        </textarea>
										    </div>
										</div>
			                            <div class="form-group">
										    <label for="Marca" class="control-label col-md-3">Marca:</label>
										    <div class="col-md-9">
										        <input class="form-control" required autocomplete="off" ng-model="product.brand" type="text">
										    </div>
										</div>
										<div class="form-group">
										    <label for="Precio" class="control-label col-md-3">Precio:</label>
										    <div class="col-md-9">
										        <input class="form-control" required autocomplete="off" ng-model="product.price" type="text">
										    </div>
										</div>
										<div class="form-group">
										    <label for="Categoria" class="control-label col-md-3">Categoría:</label>
										    <div class="col-md-9">
										        <input class="form-control" required autocomplete="off" ng-model="product.category" type="text">
										    </div>
										</div>
										<div class="form-group">
										    <label for="Stock" class="control-label col-md-3">Stock:</label>
										    <div class="col-md-9">
										        <input class="form-control" required autocomplete="off" ng-model="product.stock" type="number">
										    </div>
										</div>
										<div class="form-group">
										    <label for="Modelo" class="control-label col-md-3">Modelo:</label>
										    <div class="col-md-9">
										        <input class="form-control" required autocomplete="off" ng-model="product.model" type="text">
										    </div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
											<br>
										        <button type="submit" class="btn btn-success pull-right"  ng-disabled="registerForm.$invalid">Guardar</button>
										    </div>										    
										</div>
							    	</form>
								</div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    {!! Html::script('assets/angular/angular.min.js') !!}
    {{ Html::script('assets/irisgps/angular-app/controllers/organization/productsCtrl.js')}}
	{{ Html::script('assets/irisgps/angular-app/services/organization/ProductsServices.js')}}
		{{ Html::script('assets/bower_components/ng-file-upload/ng-file-upload.min.js')}}
	{{ Html::script('assets/bower_components/ng-file-upload-shim/ng-file-upload.min.js')}}
@endsection