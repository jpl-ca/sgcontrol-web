@extends('layouts.web')

@section('head')
    {!! Html::script('assets/angular/angular.min.js') !!}
@endsection

@section('body-tag')
    ng-app="irisGpsApp"
@endsection

@section('content')
    <div class="container-fluid" ng-controller="FormTemplateCtrl" ng-init='loadItems()'>
        <div class="container card-box">
            <div class="col-md-12">
                {!! Html::pageHeader("Plantillas de formularios") !!}
                <div>
                    <div class="text-center margin-bottom-30">
                        <a href="{{ action('Web\FormTemplateController@create') }}" class="btn btn-primary margin-bottom-40">
                            <i class="fa fa-btn fa-map-marker"></i>
                            Nuevo formulario
                        </a>

                        <nav class="navbar navbar-default">
                          <div>
                            <form class="navbar-form navbar-left">                          
                                <div class="form-group">
                                    <select class="form-control" id="searchType" ng-model="search.searchType">
                                        <option value="title" selected="selected">Título</option>
                                        <option value="description">Description</option>
                                        <option value="status">Estado</option>
                                        <option>Todos</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Ingrese texto a buscar" 
                                    id="searchTerm" type="text" required ng-model='search.searchTerm'>
                                </div>
                                <button type="submit" class="btn btn-default" ng-click="loadItems()">Buscar</button>
                            </form>
                            <!--Begin button-->
                            <div class="dropdown pull-right">
                              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Exportar
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="">XLS</a></li>
                              </ul>
                            </div>
                            <!--End button-->
                          </div>
                        </nav>
                        <table class="table table-striped table-hover ">
                          <thead>
                            <tr>
                              <th class='text-center'>#</th>
                              <th class='text-center'>Título</th>
                              <th class='text-center'>Estado</th>
                              <th class='text-center'>Acción</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="item in items">
                              <td>@{{item.id}}</td>
                              <td>@{{item.title}}</td>
                              <td>
                                <span class='label' ng-class="{'label-success': item.state=='active', 'label-default': item.state == 'inactive'}">@{{item.state}}</span>
                              </td>
                              <td>
                                <a  ng-click='activateForm(item.id)' 
                                    data-original-title="Ver detalle" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title='Activar'>
                                    <i class="fa fa-power-off"></i>
                                </a>
                                <a  ng-href="{{ url('form-templates/') }}/@{{item.id}}" 
                                    data-original-title="Ver detalle" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a  ng-href="{{ url('form-templates/') }}/@{{item.id}}/edit" 
                                    data-original-title="Ver detalle" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                                    <i class="fa fa-edit"></i>
                                </a>  
                                <a  ng-click="destroy(item.id)" 
                                    data-original-title="Ver detalle" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                                    <i class="fa fa-trash"></i>
                                </a>  
                              </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td class="text-center" ng-hide="items.length">
                                    No hay elementos...
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                          </tbody>                    
                        </table> 
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    {{ Html::script('assets/irisgps/angular-app/controllers/organization/FormTemplateCtrl.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/organization/FormTemplateService.js') }}
@endsection