<div class="form-group">
    <label for="item_title" class="control-label col-md-3">Título</label>
    <div class="col-md-9">
        <input placeholder="Ingrese un título" class="form-control" autocomplete="off" name='item_title' ng-model="item.title" type="text">
    </div>
</div>

<div class="form-group">
    <label for="item_description" class="control-label col-md-3">Descripción</label>
    <div class="col-md-9">
        <textarea name="" id="" class='form-control' rows="2" placeholder="Ingrese la descripción del formulario" ng-model="item.description"></textarea>
    </div>
</div>

<div class='clear'></div>

<div class="form-group" ng-repeat="field in item.formFields.items  track by $index">
    <label for="item_description" class="control-label col-md-3">Campo</label>
    <div class='col-md-8'>
        <input placeholder="Esta información será ingresada por los agentes" class="form-control" autocomplete="off" name='item_title' type="text" ng-model="field.label">
    </div>
    <div class='col-md-1'>
        <button class='btn btn-sm btn-secondary' ng-click='removeItemFromFormFields($index)'>X</button>
    </div>
</div>
<div class='clear'></div>
<div class='form-group'>
    <button class='btn btn-secondary pull-right' ng-click='addChildToItems()'>+ Agregar</button>
</div>

