@extends('layouts.web')

@section('head')
    {!! Html::script('assets/angular/angular.min.js') !!}
@endsection

@section('body-tag')
    ng-app="irisGpsApp"
@endsection


@section('content')
    <div class="container-fluid" ng-controller="FormTemplateCtrl">
        <div class='card-box container'>
            <div class="row">
                <div class="col-md-11 col-md-offset-1">
                    {!! Html::pageHeader("Nuevo") !!}

                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ url()->previous() }}" class="btn btn-default pull-left margin-bottom-20">Ir atrás</a>
                        </div>
                    </div>

                    <div class="row margin-bottom-20">
                    	<div class='col-md-8 col-md-offset-2'>
                            <form method="POST" accept-charset="UTF-8" class="form-horizontal" role="form">
                            <h2>Formulario</h2>
        					@include('web.form-templates.partials.form')
                            <div class='text-center margin-top-30'>
                                <a class='btn btn-lg btn-primary' ng-click='save()'>Guardar</a>
                            </div>
                            </form>
    					</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    {{ Html::script('assets/irisgps/angular-app/controllers/organization/FormTemplateCtrl.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/organization/FormTemplateService.js') }}
@endsection