<table class="table">
    <thead>
        <tr>
            <th>Placa</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Color</th>
            <th>Estado</th>
            <th class="text-center">Acciones</th>
        </tr>
    </thead>
    <tbody>
    @forelse($vehicles as $vehicle)
        <tr>
            <td>{{ $vehicle->plate }}</td>
            <td>{{ $vehicle->brand }}</td>
            <td>{{ $vehicle->model }}</td>
            <td>{{ $vehicle->color }}</td>
            <td>
                @if($vehicle->trackable->isConnected())
                    <span id="label_{{ $vehicle->trackable->trackable_type . '_' . $vehicle->id }}" class="label label-{{ $vehicle->trackable->state == \IrisGPS\Trackable::STATE_ACTIVE ? 'success' : 'warning' }}">{{ $vehicle->trackable->state }}</span>
                    <a id="disconnect_{{ $vehicle->trackable->trackable_type . '_' . $vehicle->id }}" data-original-title="disconnect" href="javascript:void(0)" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" ng-click="disconnect('{{ $vehicle->type }}', '{{ $vehicle->plate }}', '{{ $vehicle->id }}', '{{ addslashes($vehicle->trackable->trackable_type) }}')">
                        <span class="fa fa-power-off" aria-hidden="true"></span>
                    </a>
                @else
                    <span class="label label-default">Desconectado</span>
                @endif
            </td>
            <td class="text-center">
                <a data-original-title="ver" href="{{ action('Web\TrackableController@viewVehicle', $vehicle->id) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="">
                    <span class="fa fa-eye" aria-hidden="true"></span>
                </a>
                <a data-original-title="editar" href="{{ action('Web\TrackableController@editVehicle', $vehicle->id) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="">
                    <span class="fa fa-pencil" aria-hidden="true"></span>
                </a>
                <a data-original-title="eliminar"
                   href="{{ action('Web\TrackableController@destroy', $vehicle->trackable) }}"
                   class="confirm-post btn btn-xs"
                   data-text="¿Estás seguro de eliminar esto?"
                   data-confirm-button="Si, estoy seguro"
                   data-cancel-button="No estoy seguro"
                   data-toggle="tooltip"
                   data-placement="top">
                    <span class="fa fa-trash" aria-hidden="true"></span>
                </a>
            </td>
        </tr>
    @empty
        <tr class="text-center">
            <td colspan="7">There are no Vehicles registered yet...</td>
        </tr>
    @endforelse
    </tbody>
</table>