@extends('layouts.web')

@section('head')
    {!! Html::script('assets/angular/angular.min.js') !!}
@endsection

@section('body-tag')
    ng-app="irisGpsApp"
@endsection

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container-fluid" ng-controller="trackableController">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <div class="row">
                    <div class="col-md-12" id="agents-container">
                        <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Agentes</h2>
                        <nav class="navbar navbar-default">
                        <!--Begin button-->
                        <div class="dropdown pull-right  margin-top-15 margin-right-15">
                          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Exportar
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#"ng-click="downloadXls('AGENT')">XLS</a></li>
                          </ul>
                        </div>
                        <!--End button-->                        
                          <div class="container">
                            <form class="navbar-form navbar-left ng-pristine ng-valid" role="search" method="GET" action="http://localhost:8000/trackable-assets">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Ingrese texto a buscar" name="search_text" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="search_type"><option value="first_name">Nombre</option><option value="last_name">Apellido</option></select>
                                </div>
                                <button type="submit" class="btn btn-default">Buscar</button>
                            </form>
                            @if(auth()->user()->hasPrivilege('create-trackable-agent'))
                            <a href="{{ action('Web\TrackableController@createAgent') }}" class="btn btn-primary margin-top-15 margin-right-15 pull-right">
                                <i class="fa fa-btn fa-user-secret"></i>
                                Nuevo Agente
                            </a>
                            @endif
                          </div>
                        </nav>
                        @include('web.trackables.partials.agents-table')            
                        {{ $agents->render() }}
                    </div>
                    <!-- <div class="col-md-6" id="vehicles-container">
                        <h2 class="page-header">Vehículos</h2>
                        <div class="text-center">
                           
                            <div class="dropdown pull-right">
                              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Exportar
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="#" ng-click="downloadXls('VEHICLE')">XLS</a></li>
                              </ul>
                            </div>
                            
                            @if(auth()->user()->hasPrivilege('create-trackable-vehicle'))
                            <a href="{{ action('Web\TrackableController@createVehicle') }}" class="btn btn-primary margin-bottom-30">
                                <i class="fa fa-btn fa-car"></i>
                                Nuevo vehículo
                            </a>
                            @endif
                        </div>
                        @include('web.utils.search-form', ['searchText' => $searchTextOfVehicle, 'searchType' => $searchTypeOfVehicle, 'searchTypes' => $searchTypesOfVehicle])
                        @include('web.trackables.partials.vehicles-table')
                        {{ $vehicles->render() }}
                    </div> -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    {{ Html::script('assets/irisgps/angular-app/angular.irisgps.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/TrackableService.js') }}
    <script>
        irisGpsApp.controller('trackableController', ['$scope', '$compile', '$window', 'TrackableService', function($scope, $compile, $window, TrackableService) {

            $scope.disconnect = function(type, code, trackableId, trackableClass) {
                $.confirm({
                    text: "Are you sure you want to disconnect " + type + " " + code + "?",
                    confirm: function() {
                        disconnectTrackable(trackableClass, trackableId);
                    }
                });
            };

            $scope.downloadXls = function (type) {

                var form = null;
                var downloadUrl = baseUrl + '/trackable-assets/index.xls';
               

                switch (type) {
                    case 'AGENT':
                        var form = $('#agents-container form').serialize();
                        break;

                    case 'VEHICLE':
                        var form = $('#vehicles-container form').serialize();
                        break;
                }

                if (form) {
                    downloadUrl = downloadUrl + '?' + form;
                    $window.open(downloadUrl);
                    console.log(downloadUrl);
                }
                

            }

            function disconnectTrackable(trackableClass, trackableId) {
                $scope.elementId = "label_"+trackableClass+"_"+trackableId;
                $scope.buttonElementId = "disconnect_"+trackableClass+"_"+trackableId;
                var data = {
                    type: trackableClass,
                    id: trackableId
                };

                TrackableService.disconnectAsset(data).then(function (response) {
                    document.getElementById($scope.elementId).className = document.getElementById($scope.elementId).className.replace( /(?:^|\s)label-success(?!\S)/g , '' );
                    document.getElementById($scope.elementId).className = document.getElementById($scope.elementId).className.replace( /(?:^|\s)label-warning(?!\S)/g , '' );
                    document.getElementById($scope.elementId).className += " label-default";
                    document.getElementById($scope.elementId).textContent = 'disconnected';
                    document.getElementById($scope.buttonElementId).remove();
                    displayToastr('toast-bottom-right', 'success', response);
                }, function(response) {
                    console.log(response);
                    //displayToastr('toast-bottom-right', 'error', response);
                });
            }
        }]);
    </script>

@endsection
