@extends('layouts.web')

@section('head')
    {!! Html::style('assets/bower_components/angular-chart.js/dist/angular-chart.css') !!}

    <style type="text/css">
        #information-status {
            position: absolute;
            text-align: center;
            top: 400px;
            width: 400px;
            z-index: 99999;
            background-color: rgba(255, 255, 255, 0.8);
            margin-left: auto;
            margin-right: auto;
            left: 0;
            right: 0;
        }

        #search-form {
            position: absolute;
            right: 40px;
            top: 138px;
            width: 530px;
            z-index: 99999;
            background-color: rgba(255, 255, 255, 0.8);
        }

        #search-form input{
            width: 150px;
        }

        #search-form label{
            font-size: 16px;
        }
    </style>
@endsection

@section('body-tag')
    ng-app="app"
@endsection

@section('content')
    <div class="container-fluid" ng-controller="AgentGeolocationHistory" ng-init="agentId={{$agent->id or ''}};init();">
        <div class="container  margin-top-40">
            <div class="col-md-12 card-box padding-30"  style="min-height: 700px;">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Historial de ubicaciones de {{ $agent->fullname }}</h2>
                <div>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <div id='map-container'>
                        <div id='search-form'>
                            <form class="navbar-form navbar-left" role="search">
                                    <div class="form-group">
                                        <label style='font-weight: bold;'>Desde: </label>
                                        <input class="form-control" id="start_date" placeholder="YYYY-MM-DD" autocomplete="off" value="{{$startDate or ''}}" required="1" name="start_date" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label style='font-weight: bold;'>Hasta: </label>
                                        <input class="form-control" id="end_date" placeholder="YYYY-MM-DD" autocomplete="off" value="{{$endDate or ''}}" required="1" name="end_date" type="text">
                                    </div>
                                    <button type="submit" class="btn btn-default btn-sm" ng-click="loadItems()">Buscar</button>
                            </form>
                        </div>

                        <div id='information-status'>
                            <p ng-if="loadingData" class='text-center margin-top-30'>Cargando datos...</p>
                            <p ng-if="!loadingData && !areThereData()" class='text-center margin-top-30'>No hay datos para mostrar...</p>
                        </div>

                        <div id="map" style="height: 600px"></div>
                        <div ng-class="{'hide' : loadingData}">
                            <div class='hide'>
                                Mapa
                                <select ng-model='typeOfGeolocationHistory'>
                                    <option value='snapped'>Snap to road</option>
                                    <option value='clean'>Limpio</option>
                                    <option value='raw'>Crudo</option>
                                </select>
                            </div>

                            Color de ubicaciones:
                            <select ng-model='strokeColor'>
                                <option value='@{{color.value}}' ng-repeat='color in strokeColors'>@{{color.colorName}}</option>
                            </select>
                            
                            <span class="{{ $debug ? '' : 'hide' }}">
                                Transparencia de línea:
                                <select ng-model='strokeAlpha' style='width: 50px'>
                                    <option value='@{{alpha}}' ng-repeat='alpha in strokeAlphaList'># @{{alpha}}</option>
                                </select>
                            </span>

                            Marcadores
                            <input type="checkbox" name="" ng-model="showMarkers">
                            
                            <span class="{{ $debug ? '' : 'hide' }}">
                            Límite del radio
                            <input type="text" name="" ng-model="radiousLimit" style="width: 50px">
                            </span>

                            <span class="{{ $debug ? '' : 'hide' }}">
                                Límite del Zoom
                                <input type="text" name="" ng-model="maxZoomLevel" style="width: 50px"> 
                            </span>

                            <span>
                                Líneas:
                                <input type="checkbox" name="" ng-model="showPathUsingLines">
                            </span>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    <div ng-repeat='session in items track by $index'>
                        <h3>Sessión: @{{$index+1}}</h3>
                        <table class='table'>
                            <thead>
                                <th class='text-center'>
                                    ID
                                </th>
                                <th class='text-center'>
                                    Fecha
                                </th>
                                <th class='text-center'>
                                    Latitud y Longitud
                                </th>
                                <th class='text-center'>
                                    Velocidad (Km/Hr)
                                </th>
                                <th class='text-center'>
                                    Aceleración
                                </th>

                                <th class='text-center'>
                                    Distancia recorrida Km
                                </th>

                            </thead>
                            <tbody>
                                <tr ng-repeat="pnt in session">
                                    <td class='text-center'>
                                        ID@{{pnt.id}}
                                    </td>
                                    <td class='text-center'>
                                        @{{pnt.created_at}}
                                    </td>
                                    <td class='text-center'>
                                        @{{pnt.lat}}, @{{pnt.lng}}
                                    </td>
                                    <td class='text-center'>
                                        @{{pnt.speed | number : 3}}
                                    </td>
                                    <td class='text-center'>
                                        @{{pnt.acceleration}}
                                    </td>
                                    <td class='text-center'>
                                        @{{pnt.distance}}
                                    </td>
                            </tbody>
                        </table>
                    </div>
                </div>
              </div>
              <!-- Nav tabs -->
              <ul ng-class="{'hide' : loadingData}" class="nav nav-tabs {{ $debug ? '' : 'hide' }}" role="tablist" style='align: text-left'>
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Mapa</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Lista</a></li>
              </ul>
            </div>


            </div>
        </div>
    </div>
@endsection

@section('javascript')
    @include('web.includes.google-maps')
    {!! Html::script('assets/angular/angular.min.js') !!}
    {{ Html::script('assets/irisgps/angular-app/controllers/organization/AgentGeolocationHistoryCtrl.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/organization/Agent.js') }}
@endsection