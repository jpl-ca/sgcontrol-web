<div class="row margin-bottom-20">
    <div class="col-md-12">
        {!! Form::fhText('first_name', 'Nombres', $agent->first_name, ['readonly' => true]) !!}

        {!! Form::fhText('last_name', 'Apellidos', $agent->last_name, ['readonly' => true]) !!}

        {!! Form::fhText('authentication_code', 'Código de autenticación', $agent->authentication_code, ['readonly' => true]) !!}
    </div>
</div>