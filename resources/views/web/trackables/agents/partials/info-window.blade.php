<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $agent->full_name }}</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="description-row">
                    <label>Estado:</label>
                    <p>{{ $agent->trackable->state }}</p>
                </div>
                <div class="description-row">
                    <label>Fecha de la última ubicación:</label>
                    <p>{{ $agent->trackable->lastGeolocationDateTime() }}</p>
                </div>
                <div class="description-row">
                    <label>Frecuencia de transmisión:</label>
                    <p>{{ $agent->trackable->transmissionFrequency() }} seconds</p>
                </div>

                @if($agent->trackable->hasPath())
                <div class="description-row">
                    <label>Recorrido:</label>
                    <p>
                        <a target="_blank" data-original-title="ver historial de ubicaciones" href="{{ url('/trackable-assets/agent/geolocation-history/' . $agent->id . '?endDate=' . $agent->trackable->lastGeolocationDateTime()->toDateTimeString() )}}" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top">
                        Ver
                    </a>
                </p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>