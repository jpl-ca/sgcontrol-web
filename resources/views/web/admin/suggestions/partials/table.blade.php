<table class="table">
    <thead>
    <tr>
        <th>#</th>
        <th>Contenido</th>
        <th class="text-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    @forelse($items as $item)
        <tr>
            <th>{{ $item->id  }}</th>
            <td class='text-left'>
            	<p>{{ $item->content }}</p>
				<div style='font-size: 0.8em;'>
					<i class='fa fa-clock-o'></i> Enviado el {{ $item->created_at }} 
          <i class='fa fa-user'></i> por: {{ ($item->user ? $item->user->name : 'Anónimo') }}

				</div>
            </td>
            <td class="text-center">
                <a data-original-title="eliminar"
                   href="{{ action('Web\Admin\SuggestionController@destroy', $item->id) }}"
                   class="confirm-post btn btn-xs"
                   data-text="¿Estás seguro de eliminar esto?"
                   data-confirm-button="Sí, estoy seguro"
                   data-cancel-button="No estoy seguro"
                   data-toggle="tooltip"
                   data-placement="top">
                    <span class="fa fa-trash" aria-hidden="true"></span>
                </a>

            </td>
        </tr>
    @empty
        <tr class="text-center">
            <td colspan="5">No hay registros para mostrar...</td>
        </tr>
    @endforelse
    </tbody>
</table>