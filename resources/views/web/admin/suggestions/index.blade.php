@extends('layouts.web-admin')
@section('content')
<style>
    footer{
       margin-top: 83px;
    }
</style>
    <div class="container" ng-controller="trackableController">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">

                <ol class="breadcrumb margin-top-10">
                  <li><a href="/admin">Inicio</a></li>
                  <li class='active'>Sugerencias</li>
                </ol>

                <h2 class="page-header">
                  <img src="{{ url('assets/irisgps/img-web/flecha.png') }}" alt="">
                  Sugerencias
                </h2>

                <div class="text-center">
                    @include('web.utils.search-form')
                    @include('web.admin.suggestions.partials.table')
                    {{ $items->render() }}
                </div>

            </div>
        </div>
    </div>
@endsection