@extends('layouts.web-admin')

@section('content')
    <div class="container">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <ol class="breadcrumb margin-top-10">
                  <li><a href="/admin">Inicio</a></li>
                  <li><a href="/admin/organizations">Organizaciones</a></li>
                  <li class='active'>Edición</li>
                </ol>
                
                <h2 class="page-header">
                  <img src="{{ url('assets/irisgps/img-web/flecha.png') }}" alt="">
                  Edición de Organización
                  <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a>
                </h2>

                {!! Form::fhOpen(['action' => ['Web\Admin\OrganizationController@update', $organization], 'method' => 'PUT']) !!}

                {!! Form::token() !!}

				{!! Form::fhText('ruc', 'RUC', $organization->ruc, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('name', 'Nombre', $organization->name, ['autocomplete' => 'off']) !!}
                {!! Form::fhText('subscription_ends', 'Suscripción finaliza', $organization->subscription_ends, ['autocomplete' => 'off']) !!}
                <div class='form-group'>
                    <label for="subscription_id" class='control-label col-md-4'>Tipo de suscripción</label>
                    <div class='col-md-6'>
                    {!! Form::select('subscription_id', $typesOfSubscription, $organization->subscription_id, ['class' => 'form-control']) !!}
                    </div>
                </div>

                {!! Form::fhSubmit('Actualizar usuario', 'primary', 'check') !!}

                {!! Form::fhClose() !!}

            </div>
        </div>
    </div>
@endsection
