@extends('layouts.web')

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-8 card-box padding-30 col-md-offset-2">
                <ol class="breadcrumb margin-top-10">
                  <li><a href="/admin">Inicio</a></li>
                  <li><a href='/admin/organizations'>Organizaciones</a></li>
                  <li><a href="{{ action('Web\Admin\OrganizationController@view', ['organization_id' => $organization->id]) }}">{{ $organization->id }}</a></li>
                  <li><a href="{{ action('Web\Admin\OrganizationPaymentController@index', ['organization_id' => $organization->id, 'format' => 'html']) }}">Pagos</a></li>
                  <li class='active'>Nuevo pago</li>
                </ol>
                
                <h2 class="page-header">
                    <img src="{{ url('assets/irisgps/img-web/flecha.png') }}" alt="">
                    Nuevo Pago
                    <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a>
                </h2>

                {!! Form::open(['action' => ['Web\Admin\OrganizationPaymentController@store', $organization->id] , 'method' => 'POST', 'files' => true]) !!}

                    {!! Form::token() !!}

                    {!! Form::fhText('amount', 'Monto S/.', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhText('date', 'Fecha de pago', null, ['autocomplete' => 'off', 'class' => 'input-date form-control']) !!}

                    {!! Form::fhText('subscription_start_date', 'Inicia suscripción', null, ['autocomplete' => 'off', 'class' => 'input-date form-control']) !!}

                    {!! Form::fhText('subscription_end_date', 'Expira suscripción', null, ['autocomplete' => 'off', 'class' => 'input-date form-control']) !!}
                    
                    <div>
                        <label for="image" class='col-md-4'>Imagen</label>
                        <div class='col-md-6'>
                            {!! Form::file('image') !!}
                        </div>
                    </div>
                    

                    <div class='form-group clearfix'>
                        <label for="additional_information" class='col-md-4'>Información adicional</label>
                        <div class='col-md-6'>
                        {!! Form::text('additional_information', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    
                    <div classs='form-group' style='margin-top:20px;'>
                    {!! Form::fhSubmit('Crear nuevo usuario', 'primary', 'check') !!}
                    </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection
