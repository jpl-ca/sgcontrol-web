<div class="row margin-bottom-20">
    <div class="col-md-12">
        {!! Form::fhText('date', 'Fecha', $item->date, ['readonly' => true]) !!}

        {!! Form::fhText('amount', 'Monto S/.', $item->amount, ['readonly' => true]) !!}

        {!! Form::fhText('subscription_start_date', 'Inicio suscripción', $item->subscription_start_date, ['readonly' => true]) !!}

        {!! Form::fhText('subscription_end_date', 'Fin suscripción', $item->subscription_end_date, ['readonly' => true]) !!}

        {!! Form::fhText('additional_information', 'Información adicional', $item->additional_information, ['readonly' => true]) !!}

        <div class='image_responsive'>
        	<img src="{{ $item->image_url }}" alt=""/>
        </div>
    </div>
</div>