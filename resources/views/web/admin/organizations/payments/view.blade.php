@extends('layouts.web')

@section('content')
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <ol class="breadcrumb margin-top-10">
                  <li><a href="/admin">Inicio</a></li>
                  <li><a href='/admin/organizations'>Organizaciones</a></li>
                  <li><a href="{{ action('Web\Admin\OrganizationController@view', ['organization_id' => $organization->id]) }}">{{ $organization->name }}</a></li>
                  <li class='active'>Pagos</li>
                </ol>
                
                <h2 class="page-header">
                    <img src="{{ url('assets/irisgps/img-web/flecha.png') }}" alt="">
                    Pago
                    <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a>
                </h2>

                <div class="row margin-bottom-20">
                    <div class="col-md-12">

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Información</a></li>
                        </ul>

                    </div>
                </div>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="info">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @include('web.admin.organizations.payments.partials.information')
                                <a href="{{ action('Web\Admin\OrganizationPaymentController@edit', ['organization_id' => $item->organization_id, 'paymentId' => $item->id]) }}" class="btn btn-primary margin-bottom-20">Editar</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    @include('web.includes.google-maps')
    {!! Html::script('assets/irisgps/js/maps.js') !!}
    {!! Html::script('assets/irisgps/js/geolocation.js') !!}

    <script type="text/javascript">
        $(document).ready(function() {
            $('a[data-toggle="tab"][href="#geolocation"]').on('shown.bs.tab', function (e) {
                initGeolocation();
            });
        });
    </script>
@endsection
