<table class="table">
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Suscripción</th>
        <th>Estado</th>
        <th class="text-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    @forelse($organizations as $organization)
        <tr>
            <td class='text-left'>{{ $organization->name }}</td>
            <td class='text-left'>{{ $organization->subscription_id }}</td>
            <td class='text-left'>
              @if ($organization->locked)
                <label class='label label-default'><i class='fa fa-lock'></i> Bloqueado</label>
              @else
                <label class='label label-success'><i class='fa fa-check'></i> Activo</label>
              @endif
            </td>
            <td class="text-center">
                <a data-original-title="ver" href="{{ action('Web\Admin\OrganizationController@view', $organization) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-eye" aria-hidden="true"></span>
                </a>

                <a data-original-title="ver usuarios" href="{{ action('Web\Admin\OrganizationUserController@index', ['organization_id' => $organization, 'format' => 'html']) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-users" aria-hidden="true"></span>
                </a>

                <a data-original-title="ver pagos" href="{{ action('Web\Admin\OrganizationPaymentController@index', ['organization_id' => $organization, 'format' => 'html']) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-money" aria-hidden="true"></span>
                </a>
                
                <a data-original-title="editar" href="{{ action('Web\Admin\OrganizationController@edit', $organization) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-pencil" aria-hidden="true"></span>
                </a>
                
                  @if ($organization->locked)
                  <a data-original-title="Desbloquear"
                     href="{{ action('Web\Admin\OrganizationController@unlock', [$organization]) }}"
                     class="confirm-post btn btn-xs"
                     data-text="¿Estás seguro de desbloquear esta organización?"
                     data-confirm-button="Sí, estoy seguro"
                     data-cancel-button="No estoy seguro"
                     data-toggle="tooltip"
                     data-placement="top">
                      <span class="fa fa-unlock" aria-hidden="true"></span>
                  </a>
                @else
                  <a data-original-title="bloquear"
                   href="{{ action('Web\Admin\OrganizationController@lock', [$organization]) }}"
                   class="confirm-post btn btn-xs"
                   data-text="¿Estás seguro de bloquear esta organización?"
                   data-confirm-button="Sí, estoy seguro"
                   data-cancel-button="No estoy seguro"
                   data-toggle="tooltip"
                   data-placement="top">
                    <span class="fa fa-lock" aria-hidden="true"></span>
                  </a>
                @endif

                <a data-original-title="eliminar"
                   href="{{ action('Web\Admin\OrganizationController@destroy', $organization) }}"
                   class="confirm-post btn btn-xs"
                   data-text="¿Estás seguro de eliminar esto?"
                   data-confirm-button="Sí, estoy seguro"
                   data-cancel-button="No estoy seguro"
                   data-toggle="tooltip"
                   data-placement="top">
                    <span class="fa fa-trash" aria-hidden="true"></span>
                </a>

            </td>
        </tr>
    @empty
        <tr class="text-center">
            <td colspan="4">No hay organizaciones registradas aún...</td>
        </tr>
    @endforelse
    </tbody>
</table>