<div class="row margin-bottom-20">
    <div class="col-md-12">
        {!! Form::fhText('first_name', 'Nombres', $user->name, ['readonly' => true]) !!}

        {!! Form::fhText('last_name', 'Apellidos', $user->email, ['readonly' => true]) !!}

        {!! Form::fhText('type', 'Tipo', $user->type->name, ['readonly' => true]) !!}
    </div>
</div>