@extends('layouts.web-admin')

@section('content')
<style>
  footer{
    position: absolute !important;
  }
</style>
    <div class="container" ng-controller="trackableController">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <ol class="breadcrumb margin-top-10">
                  <li><a href="/admin">Inicio</a></li>
                  <li><a href="/admin/organizations">Organizaciones</a></li>
                  <li><a href="/admin/organizations/{{ $organization->id }}">{{ $organization->name}}</a></li>
                  <li class="active">Usuarios</li>
                </ol>
                
                <h2 class="page-header">
                  <img src="{{ url('assets/irisgps/img-web/flecha.png') }}" alt="">
                  Usuarios y roles
                </h2>

                <nav class="navbar navbar-default">
                    <!--Begin button-->
                        <div class="dropdown pull-right margin-top-15 margin-right-15">
                          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Exportar
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="{{ url ('/users/index.xls?page=' . $users->currentPage() ) }}">XLS</a></li>
                          </ul>
                        </div>
                    <!--End button-->

                    
                      <div class="container">
                        <form class="navbar-form navbar-left" role="search" method='GET' action='{{Request::url()}}'>
                            <div class="form-group">
                                {{ Form::text('search_text', (isset($searchText) ? $searchText : ''), ['class' => 'form-control', 'placeholder' => 'Ingrese texto a buscar']) }}
                            </div>
                            <div class='form-group'>
                              {{ Form::select('search_type', (isset($searchTypes) ? $searchTypes : []), (isset($searchType) ? $searchType : null), ['class' => 'form-control']) }}
                            </div>
                            <button type="submit" class="btn btn-default">Buscar</button>
                        </form>
                      </div>
                    </nav>
                    @include('web.admin.organizations.users.partials.users-table')   
                    {{ $users->render() }}
                </div>

            </div>
        </div>
    </div>
@endsection