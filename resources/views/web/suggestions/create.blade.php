@extends('layouts.web')
@section('body-tag')
    ng-app="irisGpsApp"
    ng-controller="suggestions" 
    ng-init="suggestion.organization_id='{{ auth('web')->user()->organization_id }}'; suggestion.user_id=' {{ auth('web')->user()->id }}'"
@endsection
@section('content')
<div class='container'>
	<div class='row'>
		<div class='col-md-12 text-center'>
			<h1 class='text-center margin-bottom-30'>Sugerencias</h1>
		</div>
    </div>

    <div class='row'>
    	<div style='width:50%;margin:auto'>
		{!! Form::fhOpen(['action' => 'Web\UserController@store', 'method' => 'POST']) !!}
		<div class='form-group'>
        {!! Form::hidden('organization_id', auth('web')->user()->organization_id) !!}
        {!! Form::hidden('user_id', auth('web')->user()->id) !!}
        {!! Form::textarea(
        	'content',
        	null,
        	[
        		'class' => 'form-control',
        		'size' => '10x5',
        		'placeholder' => 'Ingrese su sugerencia',
        		'ng-model' => 'suggestion.content'
        	])
        !!}
		</div>

		<div class='text-center'>
			<a  class='btn btn-lg btn-primary' ng-click='sendSuggestion()'>Enviar sugerencia</a>
		</di>
    	{!! Form::fhClose() !!}
    	</div>
	</div>
	
</div>
@endsection
@section('javascript')
{!! Html::script('assets/angular/angular.min.js') !!}
{!! Html::script('assets/irisgps/angular-app/angular.irisgps.suggestions.js') !!}
@endsection