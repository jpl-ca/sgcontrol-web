@extends('layouts.web')

<!-- Main Content -->
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Recuperar Contraseña</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    {!! Form::fhOpen(['url' => url('/password/email'), 'method' => 'POST']) !!}

                        {!! Form::token() !!}

                        {!! Form::fhText('email', 'E-mail', null, ['autocomplete' => 'off']) !!}
                        {!! Form::fhSubmit('Enviar correo de recuperación', 'primary', 'envelope') !!}

                    {!! Form::fhClose() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
