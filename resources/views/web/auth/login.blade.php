@extends('layouts.web')

@section('content')
<style>
body{
background:url('/assets/irisgps/img-web/foto_portada_2.jpg')no-repeat left top;
background-size: cover;   
}
footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-8 col-xs-1"></div>
        <div class="col-md-4 col-md-8 col-xs-10 login-panel">
            <!-- {!! Html::pageHeader("Login") !!} -->
            @if ($errors->has())
                @foreach($errors->all() as $error)
                <div class="alert alert-warning">
                    {{$error}}
                </div>
                @endforeach
            @endif

            @if (session('successful-registration'))
                <div class="alert alert-success">
                    {!! session('successful-registration') !!}
                </div>
            @endif
            
            @if (session('warning-authentication'))
                <div class="alert alert-warning">
                    {!! session('warning-authentication') !!}
                </div>
            @endif

            <h4 class="tlogin">Iniciar Sesión</h4>
            <div class="panel-body">
            {!! Form::fhOpen(['url' => url('/login'), 'method' => 'POST']) !!}

            {!! Form::token() !!}

          <!--   {!! Form::fhText('email', 'E-mail') !!}

            {!! Form::fhPassword('password', 'Password', ['autocomplete' => 'off']) !!} -->
            <div class="form-group">
                <label for="E-mail" class="control-label col-md-3 l16">E-mail</label>
                <div class="col-md-8">
                    <input class="form-control" name="email" type="text">
                </div>
            </div>
            <div class="form-group">
                <label for="Password" class="control-label col-md-3 l16">Password</label>
                <div class="col-md-8">
                    <input class="form-control" autocomplete="off" name="password" type="password" value="">
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <div class="checkbox">
                        <label class="l16">
                            <input type="checkbox" name="remember"> Mantenme conectado
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6">
<!--                     <a class="" href="{{ url('/password/reset') }}">¿Olvidaste tu contraseña?</a> -->
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-sgtask">
                        Ingresar
                    </button>
                </div>                
            </div>
            {!! Form::fhClose() !!}
            </div>
        </div>
    </div>
</div>
@endsection
