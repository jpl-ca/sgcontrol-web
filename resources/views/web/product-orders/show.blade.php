@extends('layouts.web')

@section('head')
    {!! Html::script('assets/angular/angular.min.js') !!}
    {{ Html::script('assets/irisgps/angular-app/controllers/organization/ordersCtrl.js')}}
	{{ Html::script('assets/irisgps/angular-app/services/organization/OrdersServices.js')}}
@endsection

@section('body-tag')
    ng-app="app"
@endsection

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container-fluid"  ng-controller="ShowOrdersController">
    	<div class="container margin-top-40"  ng-init="order_id='{{$id}}'">
            <div class="col-md-8  col-md-offset-2 card-box padding-30">
                <!-- {!! Html::pageHeader("Pedido") !!} -->
                <h1 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Pedidos #{{$id}}
                <a href="{{ action('Web\ProductOrderController@index') }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a></h1>

                <div class="row margin-bottom-20">
                    <div class="col-md-12">

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#cabecera" aria-controls="cabecera" role="tab" data-toggle="tab">Cabecera</a></li>
                            <li role="presentation"><a href="#detalle" aria-controls="detalle" role="tab" data-toggle="tab">Detalle</a></li>
                        </ul>

                    </div>
                </div>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="cabecera">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                        			<form id="markerForm" class="ng-pristine ng-valid">
			                            <div class="form-group">
			                            	<label for="Descripción" class="control-label col-md-3">Agente:</label>
										    <div class="col-md-9">
										        <input class="form-control" autocomplete="off" readonly="1" ng-value="( '#' + order.trackable_id + ' - ' + order.trackable_fullname)" type="text">
										    </div>
										</div>
										<div class="form-group">
			                            	<label for="Descripción" class="control-label col-md-3">Cliente:</label>
										    <div class="col-md-9">
										        <input class="form-control" autocomplete="off" readonly="1" ng-value="order.visit_point.name" type="text">
										    </div>
										</div>
										<div class="form-group">
											<label for="Descripción" class="control-label col-md-3">Descripción:</label>
										    <div class="col-md-9">
										        <input class="form-control" autocomplete="off" readonly="1" ng-model="order.description" type="text">
										    </div>
										</div>
										<div class="form-group">
										    <label for="Fecha de Creación" class="control-label col-md-3">Fecha de Creación:</label>
										    <div class="col-md-9">
										        <input class="form-control" autocomplete="off" readonly="1" ng-model="order.created_at" type="text">
										    </div>
										</div>
										<div class="form-group">
										    <label for="Estado" class="control-label col-md-3">Estado:</label>
										    <div class="col-md-9">
										        <input  ng-if="order.status == 'pending'" class="form-control"
										         autocomplete="off" readonly="1" value="Pendiente" type="text">
										        <input  ng-if="order.status == 'done'" class="form-control"
										         autocomplete="off" readonly="1" value="Hecho" type="text">
										        <input  ng-if="order.status == 'confirmed'" class="form-control"
										         autocomplete="off" readonly="1" value="Confirmado" type="text">
										        <input  ng-if="order.status == 'canceled'" class="form-control"
										         autocomplete="off" readonly="1" value="Cancelado" type="text">
										    </div>
										</div>
							    	</form>
								</div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="detalle">
                        <div class="row">
                            <div class="col-md-12">
                              	<table class="table table-striped table-hover ">
									  <thead>
									    <tr>
									      <th class='text-center'>Id de Producto</th>
									      <th class='text-center'>Nombre</th>
									      <th class='text-center'>Descripción</th>
									      <th class='text-center'>Cantidad</th>
									      <th class='text-center'>Precio</th>
									      <th class='text-center'>Subtotal</th>
									    </tr>
									  </thead>
									  <tbody>
									    <tr ng-repeat="detail in order.product_order_items">
									      <td class='text-center'>@{{detail.product_id}}</td>
									      <td class='text-left'>@{{detail.name}}</td>
									      <td class='text-left'>@{{detail.description}}</td>
										  <td class='text-right'>@{{detail.quantity}}</td>									 
										  <td class='text-right'>S/. @{{detail.price | number : 2}}</td>
										  <td class='text-right'>S/. @{{detail.subtotal | number : 2}}</td> 
									    </tr>
									    <tr>
									    	<td colspan="4"></td>
									    	<td style="text-align: right;"> Total:</td>
									    	<td class='text-right'>S/. @{{getTotal() | number : 2}}</td>
									    </tr>
									  </tbody>
								</table>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
