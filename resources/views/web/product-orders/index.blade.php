@extends('layouts.web')



@section('body-tag')
    ng-app="app"
@endsection

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <h1 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Pedidos</h1>
                <div ng-controller="OrdersController">
                	<nav class="navbar navbar-default">
					  <div class="container">
					    <form class="navbar-form navbar-left">					        
					        <div class="form-group">
					        	<select class="form-control" id="searchType" ng-model="searchType">
					        		<option value="description" selected="selected">Descripción</option>
					        		<option value="status">Estado</option>
					        		<option value="all">Todos</option>
					        	</select>
					        </div>
					        <div class="form-group" ng-if="visible == 'text'">
					          	<input class="form-control" placeholder="Ingrese texto a buscar" 
					          	id="searchTerm" type="text" required>
					        </div>
					        <div class="form-group" ng-if="visible == 'state'">
					        	<select class="form-control" id="searchTerm">
					        		<option value="pending">Pendiente</option>
					        		<option value="done">Hecho</option>
					        	</select>
					        </div>
					        <button type="submit" class="btn btn-default" ng-click="searchOrders()">Buscar</button>
					    </form>
					  </div>
					</nav>
	                <table class="table table-striped table-hover ">
					  <thead>
					    <tr>
					      <th class='text-center'>#</th>
					      <th class='text-center'>Fecha</th>
					      <th class='text-center'>Cliente</th>
					      <th class='text-center'>Agente</th>
					      <th class='text-center'>Estado</th>
					      <th class='text-center'>Acción</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr ng-repeat="order in orders">
					      <td class='text-center'>@{{order.id}}</td>
					      <td class='text-center'>@{{order.updated_at}}</td>
					      <td>@{{order.visit_point.name}}</td>
					      <td>@{{order.trackable_fullname}}</td>
						  <td class='text-center'>
						  	<select class='form-control' ng-model="order.status" ng-change="changeState(order)">
						  		<option value="@{{state.machineName}}" ng-repeat="state in statesList" ng-selected="state.machineName==order.status">@{{state.label}}</option>
						  	</select>
						  </td>
					      <td class='text-center'>
                            <a  href="{{ url('product-orders/') }}/@{{order.id}}" 
                            	data-original-title="Ver detalle" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                             	<i class="fa fa-eye"></i>
                            </a>  
					      </td>
					    </tr>
					    <tr>
					    	<td></td>
					    	<td></td>
					    	<td class="text-center" ng-hide="orders.length">
					    		No hay elementos...
					    	</td>
					    	<td></td>
					    	<td></td>
					    </tr>
					  </tbody>					  
					</table> 
	                <ul class="pagination">
		                <li class="disabled"><a href="#">«</a></li>
		                <li class="active"><a href="#">1</a></li>
		                <li class="disabled"><a href="#">»</a></li>
		            </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    {!! Html::script('assets/angular/angular.min.js') !!}
    {{ Html::script('assets/irisgps/angular-app/controllers/organization/ordersCtrl.js')}}
	{{ Html::script('assets/irisgps/angular-app/services/organization/OrdersServices.js')}}
@endsection