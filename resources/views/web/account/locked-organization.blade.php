@extends('layouts.web')

@section('content')
<style>
    footer{
        margin-top: 73px;
    }
    .loked h2{
        text-align: center;
        font-size: 38px;
    }
    .loked p{
        text-align: center;
        font-size: 16px;
    }
    .loked i{
        font-size: 16em;
    }
</style>
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-8 col-md-offset-2 card-box padding-30">
            
                <div class="row loked" >
                    <h2>Su cuenta de organización ha sido bloqueada</h2>
                    <p>Contáctese con nuestro equipo de soporte para obtener más información<p>
                    <i class="fa fa-lock fa-5x" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
@endsection
