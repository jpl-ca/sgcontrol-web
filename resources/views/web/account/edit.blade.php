@extends('layouts.web')

@section('content')
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-8 col-md-offset-2 card-box padding-30">
                {!! Html::pageHeader("Editing User") !!}

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ url()->previous() }}" class="btn btn-default pull-left margin-bottom-20">Go Back</a>
                    </div>
                </div>

                {!! Form::fhOpen(['action' => ['Web\UserController@update', $user], 'method' => 'POST']) !!}

                {!! Form::token() !!}
				
				{!! Form::hidden('next_url', '/account') !!}

                {!! Form::fhText('name', 'Name', $user->name, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('email', 'Email', $user->email, ['autocomplete' => 'off']) !!}
                
                {!! Form::fhPassword('password', 'Login Password') !!}

                {!! Form::fhSubmit('Update User', 'primary', 'check') !!}

                {!! Form::fhClose() !!}

            </div>
        </div>
    </div>
@endsection
