@extends('layouts.web')

@section('head')
    {!! Html::script('assets/angular/angular.min.js') !!}
@endsection

@section('body-tag')
    ng-app="irisGpsApp"
@endsection


@section('content')
    <div class="container-fluid" ng-controller="markerController">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Cliente
                #{{$marker->id}}
                <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a>
                </h2>

                <div class="row margin-bottom-20">
                    <div class="col-md-6">
                        <form id="markerForm">
                            <input id="marker_id" type="hidden" value="{{ $marker->id }}">
                            {!! Form::fhText('name', 'Nombre', null, ['id' => 'new-marker-name', 'autocomplete' => 'off', 'readonly'=>true, 'ng-model' => 'newMarker.name']) !!}
                            {!! Form::fhText('address', 'Dirección', null, ['autocomplete' => 'off', 'readonly'=>true, 'ng-model' => 'newMarker.address']) !!}
                            {!! Form::fhText('phone', 'Teléfono', null, ['autocomplete' => 'off', 'readonly'=>true, 'ng-model' => 'newMarker.phone']) !!}
                            {!! Form::fhText('reference', 'Referencia', null, ['autocomplete' => 'off', 'readonly'=>true, 'ng-model' => 'newMarker.reference']) !!}
                            {!! Form::fhText('latitude', 'Latitud', null, ['id' => 'new-marker-lat', 'autocomplete' => 'off', 'readonly'=>true, 'readonly' => 'true', 'ng-model' => 'newMarker.latitude']) !!}
                            {!! Form::fhText('longitude', 'Longitud', null, ['id' => 'new-marker-lng', 'autocomplete' => 'off', 'readonly'=>true, 'readonly' => 'true', 'ng-model' => 'newMarker.longitude']) !!}

                        </form>
                    </div>
                    <div class="col-md-6">
                        <div id="map-new-marker"></div>
                    </div>
                </div>

                <div class="row margin-bottom-20">
                    <div class="col-md-12 text-center">
                        <a class="btn btn-primary" href="{{action('Web\MarkerController@edit', $marker)}}">
                            Editar CLiente
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('javascript')
    @include('web.includes.google-maps')
    {{ Html::script('assets/irisgps/js/maps.js') }}
    {{ Html::script('assets/irisgps/angular-app/angular.irisgps.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/MarkerService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/AgentService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/VehicleService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/TaskService.js') }}
    <script>

        irisGpsApp.controller('markerController', ['$scope', '$compile', '$window', 'MarkerService', function($scope, $compile, $window, MarkerService) {

            $scope.newMarker = {};

            getMarker();

            $scope.isSubmitted = false;

            $('#new-marker-name').focus();
            mapNewMarker = initMap('map-new-marker');
            setMapStyles(mapNewMarker, defaultMapStyle());
            mapNewMarker.setZoom(12);

            $scope.addNewMarker = function() {
                $scope.isSubmitted = true;
                MarkerService.store($scope.newMarker).then(function(response){
                    displayToastr('toast-bottom-right', 'success', response);
                    $scope.newMarker = {};
                    $window.location.replace("/markers");
                }, function(response){
                    var errors = response.data;
                    $.each(errors, function ( index, value ) {
                        displayToastr('toast-bottom-right', 'error', value);
                        $scope.isSubmitted = false;
                    });
                });
            }

            function validateLoc(map)
            {
                var lat = $scope.newMarker.latitude;
                var lng = $scope.newMarker.longitude;
                if(lat.length>0 && lng.length>0)
                {
                    var coordinate = new google.maps.LatLng(lat, lng);
                    var marker = new google.maps.Marker({
                        position: coordinate,
                        map: map,
                        animation: google.maps.Animation.DROP
                    });
                }else{
                    var marker = new google.maps.Marker({});
                }
                return marker;
            }

            function getMarker()
            {
                var id = $('#marker_id').val();
                MarkerService.marker(id).then(function (response) {
                    $scope.newMarker = {
                        name: response.name,
                        latitude: response.lat,
                        longitude: response.lng,
                        address: response.address,
                        phone: response.phone,
                        reference: response.reference,
                    };
                    $scope.apply;
                    loadMarkerOnMap({ lat: response.lat, lng: response.lng});
                    var newMarker = validateLoc(mapNewMarker);

                }, function (response) {

                });
            };
            var loadMarkerOnMap = function (marker) {

                if (!marker || !marker.lat || !marker.lng) {
                    throw new Error("Lat and Lng properties are not found");
                }

                marker.lat = parseFloat(marker.lat);
                marker.lng = parseFloat(marker.lng);

                console.log(marker);
                var options = {
                    zoom: 8,
                    center: {lat: marker.lat, lng: marker.lng},
                    streetViewControl: false,
                    mapTypeControl: false   ,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    geolocationNavigator: false
                };

                $('#new-marker-name').focus();
                
                var mapNewMarker = initMap('map-new-marker', options);
                var newMarker = validateLoc(mapNewMarker);
                
                setMapStyles(mapNewMarker, defaultMapStyle());
                mapNewMarker.setZoom(12);
            }
        }]);

    </script>
@endsection