<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"> <i class='fa fa-map'></i> Información de visita</h4>
      </div>
      <div class="modal-body">
            <div id="eventForm"><!--begin my_form2-->
                <form ng-submit="false">
                
                <div class='row'>
                    <label for="text" class='col-md-3 col-sm-3 col-xs-3'>Agente </label>
                    <select class='col-md-8 col-sm-8 col-xs-8' ng-options="trackable as trackable.label for trackable in trackables" ng-model="selectedVisitPoint.selectedTrackable"></select>
                </div>

                <div class='row'>
                    <label for="text" class='col-md-3 col-sm-3 col-xs-3'>Cliente </label>
                    <select class='col-md-8 col-sm-8 col-xs-8' ng-options="marker as marker.name for marker in markers" ng-model="selectedVisitPoint.selectedMarker"></select>
                </div>
                
                <div class='row'>
                    <label for="time" class='col-md-3 col-sm-3 col-xs-3'>Fecha </label>
                    <input type="text" name="datetime" value="" id="datetime" class='col-md-8 col-sm-8 col-xs-8' ng-model="selectedVisitPoint.datetime"/>
                </div>

                <div class='row hide'>
                    <label for="time" class='col-md-3 col-sm-3 col-xs-3'>Hora </label>
                    <input type="time" name="time" value="" id="time" class='col-md-8 col-sm-8 col-xs-8'/>
                </div>

                <div class="list hide">
                    <h5> <i class='fa fa-list'></i> Lista de Tareas</h5>
                    <div class="row">

                      <div class="col-md-12 checklist">
                        <div class="input-group">
                          <input ng-model="selectedVisitPoint.newItem" type="text" class="form-control" placeholder="Escriba una tarea" ng-enter="addItemToVisitPoint()" ng-keydown="$event.which === 13 && addItemToVisitPoint()">
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button" ng-click="addItemToVisitPoint()">+</button>
                          </span>
                        </div><!-- /input-group -->
                      </div><!-- /.col-lg-6 -->
                    </div><!-- /.row -->
                    
                    <!--Checklist-->
                    <ul class='list-group'>
                        <li class='list-group-item' ng-repeat="item in selectedVisitPoint.checklist">
                            <span ng-click="removeItemToSelectedVisitPoint($index)">
                                <i class="fa fa-times remove-item-to-checklist pull-right"></i>
                            </span>
                            <label>@{{item.description}}</label>
                        </li>
                    </ul><!--End checklist-->

                </div>                
                <div class="options text-right margin-top-30">
                    <input class='btn btn-primary'type="button" name="save" value="Guardar" id="save" style='width:100px;' ng-click="saveSelectedVisitPoint()" ng-disabled="!formReadyToRegister()">

                    <input class='btn 'type="button" name="delete" value="Eliminar" id="delete" style='width:100px;' ng-click="destroySelectedVisitPoint()">
                </div>
                </form>
            </div><!--End my form2 -->



      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

