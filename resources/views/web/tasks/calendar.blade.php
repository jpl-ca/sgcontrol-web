@extends('layouts.web')

@section('head')
    {!! Html::script('assets/angular/angular.min.js') !!}
    {{ Html::script('assets/irisgps/angular-app/controllers/task.controller.js')}}
    {{ Html::script('assets/irisgps/angular-app/services/AgentService.js')}}
    {{ Html::script('assets/irisgps/angular-app/services/TaskService.js')}}
    {{ Html::script('assets/codebase/dhtmlxscheduler.js')}}
    {{ Html::script('assets/codebase/locale/locale_es.js')}}
    {{ Html::script('assets/codebase/ext/dhtmlxscheduler_timeline8c94.js?v=4.1.js')}}
    {{ Html::script('assets/codebase/ext/dhtmlxscheduler_limit.js')}}
    {{ Html::script('assets/codebase/ext/dhtmlxscheduler_serialize.js')}}
    {!! Html::style('assets/codebase/dhtmlxscheduler_flat.css') !!}
    {!! Html::style('assets/irisgps/css/calendar.css') !!}
@endsection

@section('body-tag')
    ng-app="app"
@endsection

@section('content')
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <h1 class="page-header">Programación de visitas</h1>
                <div  ng-controller="taskController">
                    <div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:100%;'>
                        <div class="dhx_cal_navline">
                            <div class="dhx_cal_prev_button">&nbsp;</div>
                            <div class="dhx_cal_next_button">&nbsp;</div>
                            <div class="dhx_cal_today_button">Hoy</div>
                            <div class="dhx_cal_date"></div>
                            <div style="left:10px; top: 17px;" class=''>
                                <ul class='list-inline list-visit-states-labels'>
                                    <li class='scheduled'>Programado</li>
                                    <li class='rescheduling-request'>Reprogramado</li>
                                    <li class='done'>Terminado</li>
                                </ul>
                            </div>
                        </div>
                        <div class="dhx_cal_header">
                        </div>
                        <div class="dhx_cal_data">
                        </div>      
                    </div>

                    @include('web.tasks.calendar.event_form')
            </div>

        </div>
        <div>
            
        </div>
    </div>
@endsection