<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Asignado a</th>
            <th>Descripción</th>
            <th>Inicia</th>
            <th>Finaliza</th>
            <th class="text-center">Acciones</th>
        </tr>
    </thead>
    <tbody>
    @forelse($tasks as $task)
        <tr>
            <th>{{ $task->id  }}</th>
            <td>{{ ucfirst($task->taskable->type) . ' - ' . $task->taskable->full_name }}</td>
            <td>{{ str_limit($task->description, 20) }}</td>
            <td>{{ $task->start_date }}</td>
            <td>{{ $task->end_date }}</td>
            <td class="text-center">
                @if(auth()->user()->hasPrivilege('view-task'))
                <a data-original-title="ver" href="{{ action('Web\TaskController@view', $task->id) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-eye" aria-hidden="true"></span>
                </a>
                @endif
                @if(auth()->user()->hasPrivilege('edit-task'))
                <a data-original-title="editar" href="{{ action('Web\TaskController@edit', $task->id) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-pencil" aria-hidden="true"></span>
                </a>
                @endif
                @if(auth()->user()->hasPrivilege('destroy-task'))
                <a data-original-title="eliminar"
                   href="{{ action('Web\TaskController@destroy', $task) }}"
                   class="confirm-post btn btn-xs"
                   data-text="¿Estás seguro de eliminar esto?"
                   data-confirm-button="Sí, estoy seguro"
                   data-cancel-button="No estoy seguro"
                   data-toggle="tooltip"
                   data-placement="top">
                    <span class="fa fa-trash" aria-hidden="true"></span>
                </a>
                @endif
            </td>
        </tr>
    @empty
        <tr class="text-center">
            <td colspan="6">There are no Tasks registered yet...</td>
        </tr>
    @endforelse
    </tbody>
</table>