<div id="map" class="margin-bottom-20"></div>
<div class="hidden map-markers">
    @if($task->hasGeolocation())
        <div data-role="marker" data-lat="{{ $task->lastGeolocation()->lat }}" data-lng="{{ $task->lastGeolocation()->lng }}" data-title="{{ $task->taskable->full_name }}" data-status="inactive" data-icon="{{ $task->taskable->trackable->getIcon() }}" data-info-window="{{ $task->getTrackableInfoWindow() }}"></div>
    @endif
    @foreach($task->visitPoints as $visitPoint)
        @if($visitPoint->visit_state_id != \IrisGPS\VisitState::STATE_CANCELLED)
            <div data-role="task-marker" data-lat="{{ $visitPoint->lat }}" data-lng="{{ $visitPoint->lng }}" data-title="{{ $visitPoint->name }}" data-status="{{ $visitPoint->status }}" data-icon="{{ $visitPoint->getIcon() }}" data-info-window="{{ $visitPoint->getInfoWindow() }}"></div>
        @endif
    @endforeach
</div>
<div class="hidden map-polylines">
    <div class="polyline">
        @foreach($task->geolocationHistory()->orderBy('created_at', 'asc')->get() as $point)
            <div data-role="polyline-point" data-lat="{{ $point->lat }}" data-lng="{{ $point->lng }}"></div>
        @endforeach
    </div>
</div>