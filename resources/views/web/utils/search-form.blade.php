<nav class="navbar navbar-default">
  <div class="container">
    <form class="navbar-form navbar-left" role="search" method='GET' action='{{Request::url()}}'>
        <div class="form-group">
          	{{ Form::text('search_text', (isset($searchText) ? $searchText : ''), ['class' => 'form-control', 'placeholder' => 'Ingrese texto a buscar']) }}
        </div>
        <div class='form-group'>
        	{{ Form::select('search_type', (isset($searchTypes) ? $searchTypes : []), (isset($searchType) ? $searchType : null), ['class' => 'form-control']) }}
        </div>
        <button type="submit" class="btn btn-default">Buscar</button>
    </form>
  </div>
</nav>