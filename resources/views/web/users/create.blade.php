@extends('layouts.web')

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30"> 
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Nuevo Usuario
                <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a>
                </h2>



                {!! Form::fhOpen(['action' => 'Web\UserController@store', 'method' => 'POST']) !!}

                    {!! Form::token() !!}

                    {!! Form::fhText('name', 'Nombre', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhText('email', 'Correo', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhPassword('password', 'Contraseña') !!}

                    {!! Form::fhSubmit('Crear nuevo usuario', 'primary', 'check') !!}

                {!! Form::fhClose() !!}

            </div>
        </div>
    </div>
@endsection
