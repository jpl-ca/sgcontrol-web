<table class="table">
    <thead>
    <tr>
        <th>Correo</th>
        <th>Nombre</th>
        <th>Tipo</th>
        <th class="text-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    @forelse($users as $user)
        <tr>
            <th class='text-left'>{{ $user->email  }}</th>
            <td class='text-left'>{{ $user->name }}</td>
            <td class='text-left'>{{ $user->type->nameToSpanish }}</td>
            <td class="text-center">
                @if($hasPrivilegesToView)
                <a data-original-title="ver" href="{{ action('Web\UserController@view', $user) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-eye" aria-hidden="true"></span>
                </a>
                @endif
                @if($hasPrivilegesToEdit)
                <a data-original-title="editar" href="{{ action('Web\UserController@edit', $user) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-pencil" aria-hidden="true"></span>
                </a>
                @endif

                @if ($hasPrivilegesToDestroy)
                <a data-original-title="eliminar"
                   href="{{ action('Web\UserController@destroy', $user) }}"
                   class="confirm-post btn btn-xs"
                   data-text="¿Estás seguro de eliminar esto?"
                   data-confirm-button="Sí, estoy seguro"
                   data-cancel-button="No estoy seguro"
                   data-toggle="tooltip"
                   data-placement="top">
                    <span class="fa fa-trash" aria-hidden="true"></span>
                </a>
                @endif
            </td>
        </tr>
    @empty
        <tr class="text-center">
            <td colspan="5">There are no Users registered yet...</td>
        </tr>
    @endforelse
    </tbody>
</table>