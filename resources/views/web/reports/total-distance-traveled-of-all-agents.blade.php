@extends('layouts.web')

@section('head')
    {!! Html::style('assets/bower_components/angular-chart.js/dist/angular-chart.css') !!}
@endsection

@section('body-tag')
    ng-app="app"
@endsection

@section('content')
    <div class="container-fluid" ng-controller="ReportDistanceTraveledOfAllAgents" ng-init="getItems()">
        <div class="container  margin-top-40">
            <div class="col-md-12 card-box padding-30"  style="min-height: 500px;">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Reporte de la distancia recorrida de los agentes</h2>
                <div>
                    <nav class="navbar navbar-default">
                      <div class="container-fluid">
                        <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <label for="">Desde: </label>
                                <input class="form-control" id="start_date" value="{{$startDate}}" placeholder="YYYY-MM-DD" autocomplete="off" required="1" name="start_date" type="text">
                            </div>
                            <div class="form-group">
                                <label for="">Hasta: </label>
                                <input class="form-control" id="end_date" placeholder="YYYY-MM-DD" autocomplete="off" value="{{$endDate}}" required="1" name="end_date" type="text">
                            </div>
                            <button type="submit" class="btn btn-default" ng-click="getItems()">Buscar</button>
                            
                        </form>
                        <div class="dropdown pull-right margin-top-15 margin-right-15">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              Exportar
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li><a href="#" ng-click="export('xls')">XLS</a></li>
                            </ul>
                        </div>
                      </div>
                    </nav>
                </div>
                <p ng-class="{'hide' : (items.length > 0)}" class='text-center margin-top-30'>Al parecer no se ha registrado ningún agente. Debe tener al menos 1 agente registrado</p>
                <table class='table margin-top-30' ng-class="{'hide' : !(items.length > 0)}">
                    <thead>
                        <th class='text-center'>#</th>
                        <th class='text-center'>Agente</th>
                        <th class='text-center'>Recorrido en Km.</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr ng-repeat="item in items">
                            <td class='text-center'>@{{$index + 1}}</td>
                            <td>@{{item.agentFullName}}</td>
                            <td class='text-center'>@{{item.distance | number: 2}}</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    {!! Html::script('assets/angular/angular.min.js') !!}
    {{ Html::script('assets/irisgps/angular-app/controllers/organization/ReportDistanceTraveledOfAllAgentsCtrl.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/organization/reportsServices.js') }}
@endsection