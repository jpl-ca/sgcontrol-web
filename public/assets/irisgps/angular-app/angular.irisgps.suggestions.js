var irisGpsApp = angular.module('irisGpsApp', []);
irisGpsApp.controller('suggestions', ['$scope', '$http', function ($scope, $http) {

	$scope.sendSuggestion = function () {

		var data =  $scope.suggestion;
		var url = baseUrl + '/api/suggestions';
		console.log('sending...');
		console.log(data);
		$http({
			method: 'POST',
			url: url,
			data: data
		})
		.success(function (response) {
			console.log(response);
			displayToastr('toast-bottom-full-width', 'success',response.message);
			$scope.suggestion.content = "";
		})
		.error(function (response, codeStatus) {
			console.log(response);
			var message = "";
			if (codeStatus > 400 && codeStatus < 500) {
				message += response.message;
				for (var i in response.errors) {
					for (var j in response.errors[i]) {
						message += "\n " + response.errors[i][j];
					}
				}
			}
			displayToastr('toast-bottom-full-width', 'error', message);
			
		});

		return false;
	}
}]);