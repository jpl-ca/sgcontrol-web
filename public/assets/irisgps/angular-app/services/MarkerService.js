irisGpsApp.factory('MarkerService', function($http) {
    return {
        countries: function () {
            return $http.get(baseUrl + '/assets/irisgps/js/countries.json').then(function (response) {
                return response.data;
            });
        },
        markers: function () {
            return $http.get(baseUrl + '/api/markers').then(function (response) {
                return response.data;
            });
        },
        marker: function (id) {
            return $http.get(baseUrl + '/api/markers/'+id).then(function (response) {
                return response.data;
            });
        },
        store: function (marker) {
            return $http.post(baseUrl + '/api/markers/store', marker).then(function (response) {
                return response.data;
            });
        },
        update: function (marker) {
            return $http.post(baseUrl + '/api/markers/update', marker).then(function (response) {
                return response.data;
            });
        }
    };
});