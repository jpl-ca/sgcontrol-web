app.factory('Agent', function($http) {
    return {
        getGeolocationHistory: function (agentId, startDate, endDate, type) {
            var url = baseUrl + '/api/auth/organization/me/agents/'+ agentId + '/geolocation-history?startDate=' + startDate + ' &endDate=' + endDate + '&type=' + type;
            console.log("Loading history...");
            console.log(url);
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }
    };
});