app.factory('productsService', function($http) {
    return {
        getAll: function (page) {
            return $http.get(base_url +'/api/auth/organization/me/products?page=' + page).then(function (response) {
                return response.data;
            });
        }
        ,
        deleteProduct: function(id) {
            return $http.delete(baseUrl + '/api/auth/organization/me/products/' + id).then(function (response) {
                return response.data;                
            });
        }
        ,
        getProduct: function (id) {
            return $http.get(base_url +'/api/auth/organization/me/products/'+ id).then(function (response) {
                return response.data;
            });
        }
        ,
        searchProducts: function (searchTerm, searchType) {
            return $http.get(base_url +'/api/auth/organization/me/products?searchTerm='+ searchTerm +'&searchType='+ searchType).then(function (response) {
                return response.data;
            });
        }
        ,
        addProduct: function (product) {
            return $http.post(base_url +'/api/auth/organization/me/products', product).then(function (response) {
                return response.data;
            });
        }
        ,
        updateProduct: function (id, product) {
            return $http.put(base_url +'/api/auth/organization/me/products/' + id, product).then(function (response) {
                return response.data;
            });
        }
    };
});