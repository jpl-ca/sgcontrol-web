irisGpsApp.factory('FormTemplateService', function($http) {
    return {
        items: function (options) {
            var url = baseUrl + '/api/auth/organization/me/form-templates?tmp=tmp';

            if (options['search']) {
                if (options['search']['searchType'] && options['search']['searchTerm']) {
                    url += '&searchType=' +  options['search']['searchType'] + '&searchTerm=' + options['search']['searchTerm'];
                }
            }

            return $http.get(url).then(function (response) {
                return response.data;
            });
        },
        item: function (id, options) {
            var fields = null;

            if (options && options['fields']) {
                fields = options['fields'];
            }

            var url = baseUrl + '/api/auth/organization/me/form-templates/' + id;

            if (fields) {
                url += '?fields=' + fields;
            }

            return $http.get(url).then(function (response) {
                return response.data;
            });
        },
        create: function (item) {
           return $http.post(baseUrl + '/api/auth/organization/me/form-templates', item).then(function (response) {
                return response.data;
            });
        },
        update: function (item) {
            var url = baseUrl + '/api/auth/organization/me/form-templates/' + item.id;
            return $http.put(url, item).then(function (response) {
                return response.data;
            });
        },
        destroy: function (itemId) {
            var url = baseUrl + '/api/auth/organization/me/form-templates/' + itemId;
            return $http.delete(url).then(function (response) {
                return response;
            });
        },
        activate: function (itemId) {
            var url = baseUrl + '/api/auth/organization/me/form-templates/' + itemId + '/activate';
            return $http.post(url).then(function (response) {
                return response;
            });
        }
    };
});