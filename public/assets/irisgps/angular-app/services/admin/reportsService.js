app.factory('reportsService', function($http) {
    return {
        getAgentsforOrg: function (start_date, end_date) {
            return $http.get(base_url +'/api/auth/admin/me/organizations/total-of-agents?startDate='+start_date+'&endDate='+end_date).then(function (response) {
                return response.data;
            });
        },
        getOrg: function () {
            return $http.get(base_url +'/api/auth/admin/me/organizations').then(function (response) {
                return response.data;
            });
        },
        getVisitsforOrg: function (id, start_date, end_date) {
            return $http.get(base_url +'/api/auth/admin/me/organizations/'+ id +'/visits/all?startDate='+start_date+'&endDate='+end_date).then(function (response) {
                return response.data;
            });
        },
        getAllVisits: function (start_date, end_date) {
            return $http.get(base_url +'/api/auth/admin/me/organizations/visits/all?startDate='+start_date+'&endDate='+end_date).then(function (response) {
                return response.data;
            });
        }
    };
});