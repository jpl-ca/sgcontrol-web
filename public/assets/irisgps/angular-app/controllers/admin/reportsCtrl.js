'use strict';
var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module('app', []);



app.controller('ReportsController', ['$scope', '$http', 'reportsService', '$timeout', function($scope, $http, reportsService, $timeout)
{
        var html = function(id) { return document.getElementById(id); };

        $scope.getAgentsforOrg = function() {
              $scope.chart = "";
              reportsService.getAgentsforOrg(html("start_date").value, html("end_date").value)
                .then(function(data){ 
                    $scope.data = data.data;
                    if ($scope.data.length > 0) {
                        for (var i = 0; i < $scope.data.length; i++) {
                            delete $scope.data[i]["cancelled"];
                        };
                    };
              		console.log($scope.data);
        			$scope.chart = AmCharts.makeChart("chartdiv", {
                                    "type": "serial",
                                     "theme": "light",
                                    "categoryField": "name",
                                    "rotate": true,
                                    "startDuration": 1,
                                    "categoryAxis": {
                                        "gridPosition": "start",
                                        "position": "left"
                                    },
                                    "trendLines": [],
                                    "graphs": [
                                        {
                                            "balloonText": "Total:[[value]]",
                                            "fillAlphas": 0.8,
                                            "id": "AmGraph-1",
                                            "lineAlpha": 0.2,
                                            "title": "Total",
                                            "type": "column",
                                            "valueField": "total"
                                        }
                                    ],
                                    "guides": [],
                                    "valueAxes": [
                                        {
                                            "id": "ValueAxis-1",
                                            "position": "top",
                                            "axisAlpha": 0
                                        }
                                    ],
                                    "allLabels": [],
                                    "balloon": {},
                                    "titles": [],
                                    "dataProvider": $scope.data,
                                    "export": {
                                        "enabled": true,
                                        "position": "top-left", 
                                        "menu": [{
                                            "class": "export-main",
                                            "menu": [{
                                            "format": "XLSX",
                                            "label": "Exportar Excel",
                                            "title": "Exporta a Excel",
                                          },{
                                            "format": "PDF",
                                            "label": "Exportar PDF",
                                            "title": "Exporta a PDF",
                                          },{
                                            "format": "CSV",
                                            "label": "Exportar CSV",
                                            "title": "Exporta a CSV",
                                          },]
                                        }]
                                    } 
                  
                                });

           $scope.zoomChart = function(){
                                    $scope.chart.zoomToIndexes($scope.chart.dataProvider.length - 20, $scope.chart.dataProvider.length - 1);
                                }

                                $scope.chart.addListener("dataUpdated",   $scope.zoomChart);
                                $scope.zoomChart; 
            	}); 
        }


}]);


app.controller('ReportsController2', ['$scope', '$http', 'reportsService', '$timeout', function($scope, $http, reportsService, $timeout)
{
        var html = function(id) { return document.getElementById(id); };
        $scope.getOrg = function() {
             reportsService.getOrg()
                .then(function(orgs){ 
                  $scope.orgs = [];
                    $scope.orgs = orgs.data;
                    console.log($scope.orgs);
                    var all = {
                      id: 'all',
                      name: 'Todos'
                    };
                    $scope.orgs.push(all);
                    
                    $scope.selected_org = $scope.orgs[$scope.orgs.length-1];

                }); 
        }
        $scope.getOrg();
        $scope.$watch('selected_org', function(org){
            if(org){
                $scope.id = org.id; 
                console.log(document.getElementById("selected_org").value);   
            }               
        });

        $scope.getVisitsforOrg = function() {
              if ($scope.id == undefined || $scope.id == "all") {
                    $scope.chart = "";
              reportsService.getAllVisits(html("start_date").value, html("end_date").value, $scope.id)
                .then(function(data){ 
                    $scope.data = data.data;
                    if ($scope.data.length > 0) {
                        for (var i = 0; i < $scope.data.length; i++) {
                            delete $scope.data[i]["cancelled"];
                        };
                    };
                    console.log($scope.data);                    
                    $scope.chart = AmCharts.makeChart("chartdiv", {
                                    "type": "serial",
                                    "theme": "light",
                                    "legend": {
                                        "useGraphSettings": true
                                    }, 
                                    "dataProvider": $scope.data,
                                    "categoryField": "date",
                                    "synchronizeGrid":true,
                                    "valueAxes": [{
                                        "id":"v1",
                                        "axisColor": "#009486",
                                        "axisThickness": 2,
                                        "axisAlpha": 1,
                                        "position": "left"
                                    }],
                                    "chartScrollbarSettings": {
                                        "graph": "g1",
                                        "usePeriod": "10mm",
                                        "position": "top"
                                    },
                                    "graphs": [{
                                        "valueAxis": "v1",
                                        "lineColor": "#8E24AA",
                                        "bullet": "round",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Programado",
                                        "valueField":"scheduled",
                                        "fillAlphas": 0
                                    }, {
                                        "valueAxis": "v1",
                                        "lineColor": "#00BCD4",
                                        "bullet": "square",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Reprogramdo",
                                        "valueField":"rescheduled",
                                        "fillAlphas": 0
                                    }, {
                                        "valueAxis": "v1",
                                        "lineColor": "#3F51B5",
                                        "bullet": "triangleUp",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Hecho",
                                        "valueField":"done",
                                        "fillAlphas": 0
                                    }
                                    // , {
                                    //     "valueAxis": "v1",
                                    //     "lineColor": "blue",
                                    //     "bullet": "diamond",
                                    //     "bulletBorderThickness": 1,
                                    //     "hideBulletsCount": 30,
                                    //     "title":"total",
                                    //     "valueField":"total",
                                    //     "fillAlphas": 0
                                    // }
                                    ],
                                      "chartCursor": {
                                          "pan": true,
                                          "valueLineEnabled": true,
                                          "valueLineBalloonEnabled": true,
                                          "cursorAlpha":1,
                                          "cursorColor":"#258cbb",
                                          "limitToGraph":"g1",
                                          "valueLineAlpha":0.2,
                                          "valueZoomable":true
                                      },
                                    "categoryAxis": {
                                        "parseDates": true,
                                        "axisColor": "#DADADA",
                                        "minorGridEnabled": true
                                    },
                                    "export": {
                                        "enabled": true,
                                        "position": "top-right", 
                                        "menu": [{
                                            "class": "export-main",
                                            "menu": [{
                                            "format": "XLSX",
                                            "label": "Exportar Excel",
                                            "title": "Exporta a Excel",
                                          },{
                                            "format": "PDF",
                                            "label": "Exportar PDF",
                                            "title": "Exporta a PDF",
                                          },{
                                            "format": "CSV",
                                            "label": "Exportar CSV",
                                            "title": "Exporta a CSV",
                                          },]
                                        }]
                                    } 
                  
                                });

                                $scope.zoomChart = function(){
                                    $scope.chart.zoomToIndexes($scope.chart.dataProvider.length - 20, $scope.chart.dataProvider.length - 1);
                                }
                                $scope.chart.addListener("dataUpdated",   $scope.zoomChart);
                                $scope.zoomChart; 
                }); 
              }else if($scope.id != "all" || $scope.id != undefined){
                $scope.chart = "";
              reportsService.getVisitsforOrg($scope.id ,html("start_date").value, html("end_date").value, $scope.id)
                .then(function(data){ 
                    $scope.data = data.data;
                    if ($scope.data.length > 0) {
                        for (var i = 0; i < $scope.data.length; i++) {
                            delete $scope.data[i]["cancelled"];
                        };
                    };
                    console.log($scope.data);                    
                    $scope.chart = AmCharts.makeChart("chartdiv", {
                                    "type": "serial",
                                    "theme": "light",
                                    "legend": {
                                        "useGraphSettings": true
                                    }, 
                                    "dataProvider": $scope.data,
                                    "categoryField": "date",
                                    "synchronizeGrid":true,
                                    "valueAxes": [{
                                        "id":"v1",
                                        "axisColor": "#009486",
                                        "axisThickness": 2,
                                        "axisAlpha": 1,
                                        "position": "left"
                                    }],
                                    "chartScrollbarSettings": {
                                        "graph": "g1",
                                        "usePeriod": "10mm",
                                        "position": "top"
                                    },
                                    "graphs": [{
                                        "valueAxis": "v1",
                                        "lineColor": "#8E24AA",
                                        "bullet": "round",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Programado",
                                        "valueField":"scheduled",
                                        "fillAlphas": 0
                                    }, {
                                        "valueAxis": "v1",
                                        "lineColor": "#00BCD4",
                                        "bullet": "square",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Reprogramdo",
                                        "valueField":"rescheduled",
                                        "fillAlphas": 0
                                    }, {
                                        "valueAxis": "v1",
                                        "lineColor": "#3F51B5",
                                        "bullet": "triangleUp",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Hecho",
                                        "valueField":"done",
                                        "fillAlphas": 0
                                    }
                                    // , {
                                    //     "valueAxis": "v1",
                                    //     "lineColor": "blue",
                                    //     "bullet": "diamond",
                                    //     "bulletBorderThickness": 1,
                                    //     "hideBulletsCount": 30,
                                    //     "title":"total",
                                    //     "valueField":"total",
                                    //     "fillAlphas": 0
                                    // }
                                    ],
                                      "chartCursor": {
                                          "pan": true,
                                          "valueLineEnabled": true,
                                          "valueLineBalloonEnabled": true,
                                          "cursorAlpha":1,
                                          "cursorColor":"#258cbb",
                                          "limitToGraph":"g1",
                                          "valueLineAlpha":0.2,
                                          "valueZoomable":true
                                      },
                                    "categoryAxis": {
                                        "parseDates": true,
                                        "axisColor": "#DADADA",
                                        "minorGridEnabled": true
                                    },
                                    "export": {
                                        "enabled": true,
                                        "position": "top-right", 
                                        "menu": [{
                                            "class": "export-main",
                                            "menu": [{
                                            "format": "XLSX",
                                            "label": "Exportar Excel",
                                            "title": "Exporta a Excel",
                                          },{
                                            "format": "PDF",
                                            "label": "Exportar PDF",
                                            "title": "Exporta a PDF",
                                          },{
                                            "format": "CSV",
                                            "label": "Exportar CSV",
                                            "title": "Exporta a CSV",
                                          },]
                                        }]
                                    } 
                  
                                });

                                $scope.zoomChart = function(){
                                    $scope.chart.zoomToIndexes($scope.chart.dataProvider.length - 20, $scope.chart.dataProvider.length - 1);
                                }
                                $scope.chart.addListener("dataUpdated",   $scope.zoomChart);
                                $scope.zoomChart; 
                }); 
              }
        }


}]);


    $(function () {
            $('#start_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('#end_date').datetimepicker({
                    useCurrent: false,
                    format: 'YYYY-MM-DD'
            });
            $("#start_date").on("dp.change", function (e) {
                $('#end_date').data("DateTimePicker").minDate(e.date);
            });
            $("#end_date").on("dp.change", function (e) {
                $('#start_date').data("DateTimePicker").maxDate(e.date);
            });
    });
