'use strict';
var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module('app', []);



app.controller('ReportsController', ['$scope', '$http', 'reportsService', '$timeout', function($scope, $http, reportsService, $timeout)
{
        var html = function(id) { return document.getElementById(id); };
        $scope.getAgents = function() {
             reportsService.getAgents()
                .then(function(agents){ 
                  $scope.agents = [];
                    $scope.agents = agents;
                    // console.log($scope.agents);
                    // var all = {
                    //   id: 'all',
                    //   first_name: 'Todos'
                    // };
                    // $scope.agents.push(all);
                    
                    // $scope.selected_agent = agents[agents.length-1];
                }); 
        }
        $scope.getAgents();
        $scope.$watch('selected_agent', function(agent){
            if(agent){
                $scope.id = agent.id;  
            }               
        });

			$('#scheduler_here').hide();

        $scope.getVisitsforAgent = function() {
            $('#scheduler_here').show();
            $scope.mes = html("month").value;            
            $scope.start = html("month").value+'-01';
            $scope.end = moment($scope.mes).endOf('month')._d;
            scheduler.config.xml_date="%Y-%m-%d %H:%i";
            scheduler.init('scheduler_here',$scope.end,"month");
            scheduler.config.drag_move = false;
            scheduler.config.readonly_form = false;
            //block all modifications
            scheduler.attachEvent("onBeforeDrag",function(){return false;})
            scheduler.attachEvent("onClick",function(){return false;})
            scheduler.config.details_on_dblclick = false;
            scheduler.config.dblclick_create = false;
        	$scope.vpoints = "";
            reportsService.getVisits($scope.id, $scope.start, $scope.end)
            	.then(function(visits){ 
            	  scheduler.clearAll();
            		$scope.vpoints = visits.data.visit_points;
                    console.log($scope.vpoints);
            		scheduler.parse($scope.vpoints, "json");                    
            	}); 
            
        
        scheduler.showLightbox = function (id) {            
            var ev = scheduler.getEvent(id);  
             $scope.show_form_from_event(ev);
        };

        scheduler.attachEvent("onDblClick", function (id, e){
            var ev = scheduler.getEvent(id);             
            $scope.show_form_from_event(ev);
          });

        } //end load calendar
        
        $scope.exportScheduler = function(type){
            var format = "A2";
            var orient = "landscape";
            var zoom = 1;
            var filename = "Reporte de visitas del agente "+html("selected_agent").value+" en "+$scope.mes;
            var filename2 = filename.replace(/ /g, '_').replace(/-/g, '_');
            if (type == "pdf") {
                scheduler.exportToPDF({
                    name: filename2+".pdf",
                    format:format,
                    orientation:orient,
                    zoom:zoom,
                    header: '<style>.dhx_cal_event_clear {color:white !important}h1{text-align:center;color:#009486}a{color:silver}</style><h1>'+filename+'</h1>'
                });
            }else if (type == "xls") {
                scheduler.exportToExcel({
                    name: filename2+".xls", 
                    columns:[
                        { id:"text",  header:"Visita", width:50 },
                        { id:"start_date",  header:"Fecha", width:50 }
                    ]
                });
            }
        }


        $scope.show_form_from_event = function (ev) {
            $scope.resetSelectedVisitPoint();
            $('.modal').on('show.bs.modal', function (e) {
                $scope.draw_event_into_event_form(ev);
                $scope.$apply();
            });
            $('.modal').modal({show: true});
        }

        $scope.draw_event_into_event_form = function (ev) {
            console.log(ev);
            $scope.selectedVisitPoint.product_order = {
                product_order_items: []
            };

            $scope.selectedVisitPoint.time = ev.time;
            $scope.selectedVisitPoint.key = ev.key;    
                   
            if (ev.product_order != null){
                $scope.selectedVisitPoint.product_order.product_order_items = ev.product_order.product_order_items;  
              }else{
                  // ev.product_order= {
                  //       product_order_items: []
                  //   };   
              } 

            $scope.selectedVisitPoint.event_id = ev.id;
            $scope.selectedVisitPoint.name = ev.name;
            $("#eventForm #time").val(ev.time);
            
            if (!ev.datetime) {
                $scope.selectedVisitPoint.datetime = moment(ev.start_date).format("YYYY-MM-DD h:mm:ss");
            } else {
                $scope.selectedVisitPoint.datetime = ev.datetime;
            }

            $scope.selectedVisitPoint.selectedTrackable = ev.trackable_id;
            $scope.selectedVisitPoint.selectedMarker = ev.name;
        }

        $scope.resetSelectedVisitPoint = function () {
            $scope.selectedVisitPoint = {};
        }

        $scope.hideSelectedVisitPointModal = function () {        
            $('.modal').modal('hide');
        }

}]);


    $(function () {
            $('#month').datetimepicker({
                 format: 'YYYY-MM'
            });
            
            $('#start_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('#end_date').datetimepicker({
                    useCurrent: false,
                    format: 'YYYY-MM-DD'
            });
            $("#start_date").on("dp.change", function (e) {
                $('#end_date').data("DateTimePicker").minDate(e.date);
            });
            $("#end_date").on("dp.change", function (e) {
                $('#start_date').data("DateTimePicker").maxDate(e.date);
            });
    });

    