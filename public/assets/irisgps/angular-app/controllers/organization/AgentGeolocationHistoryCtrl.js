'use strict';
var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module('app', []);

app.controller('AgentGeolocationHistory', ['$scope', '$http', 'Agent', '$timeout', function($scope, $http, Agent, $timeout)
{
    var map = null;
    $scope.loadingData = false;
    $scope.typeOfGeolocationHistory = 'clean';
    $scope.showMarkers = true;
    $scope.maxZoomLevel = 17;
    $scope.mapZoom = $scope.maxZoomLevel - 3;
    $scope.strokeColors = [
        {
            value: "#E21D24",
            colorName: "Rojo"    
        },
        {
            value: "#FFA526",
            colorName: "Naranja"    
        },
        {
            value: "#3A9ED3",
            colorName: "Celeste"    
        },
        {
            value: "#FCE25F",
            colorName: "Amarillo"
        },
        {
            value: "#B1A9A2",
            colorName: "Morado"
        },
        {
            value: "#3DB547",
            colorName: "Verde"
        }
    ];

    $scope.strokeAlphaList = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];
    $scope.strokeColor = $scope.strokeColors[0].value;
    $scope.strokeAlpha = $scope.strokeAlphaList[4];
    $scope.strokeScale= 5;
    $scope.animateLastPoint = true;
    $scope.animateFirstPoint = false;
    $scope.strokeRepeat = '15px';
    $scope.strokeOpacityOfMarkers = $scope.strokeAlphaList[4];
    $scope.radiousLimit = 15;
    $scope.showPathUsingLines = true;

    $scope.loadItems = function() {
        if ($scope.loadingData) {
            console.log("Ya hay una conexión de carga establecida");
            return false;
        }
        var startDate = document.getElementById('start_date') ? document.getElementById('start_date').value : '';
        var endDate = document.getElementById('end_date') ? document.getElementById('end_date').value : '';

        if (endDate && endDate.length == 10) {
            endDate = moment(endDate).add(1, 'days').add(-1, 'seconds').format('YYYY-MM-DD HH:mm:ss');
        }

        if (!startDate && endDate) {
            startDate = moment(endDate).startOf('day').format('YYYY-MM-DD');
        }

        $scope.loadingData = true;
        $('#map').addClass('hide');
        Agent.getGeolocationHistory($scope.agentId, startDate, endDate, $scope.typeOfGeolocationHistory).then(function (data) {
            console.log(data);
            $scope.loadingData = false;
            $scope.items = data.data;

            $('#map').removeClass('hide');
            $scope.createMap();
            map.setCenter({ lat:-12.0879227, lng:-77.0159834 });
            if ($scope.areThereData()) {
                $scope.clearAndDrawLines();
            }
        });
    }

    $scope.createMap = function () {
        var maxZoomLevel = $scope.maxZoomLevel;

        map = new google.maps.Map(document.getElementById('map'), {
            center: { lat:-12.0879227, lng:-77.0159834 },
            zoom: $scope.mapZoom,
            center: {lat:-12.0879227, lng:-77.0159834},
            streetViewControl: false,
            mapTypeControl: false   ,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: defaultMapStyle() 
        });

        google.maps.event.addListener(map, 'zoom_changed', function() {
            console.log('Zoom changed');
            if (map.getZoom() > maxZoomLevel) {
                map.setZoom(maxZoomLevel);   
            }
        });
    }

    $scope.clearAndDrawLines = function () {
        var items = $scope.removePointsClose($scope.items);

        if ($scope.showPathUsingLines) {
            $scope.drawPathUsingLines(items);
        }

        if ($scope.showMarkers) {
            $scope.drawMarkers(items);    
        }

        if (items) {
            var lastIndex = items.length-1;
            var lastPoint = items[lastIndex][items[lastIndex].length-1];
            var firstPoint = items[0][0];
            // Setting zoom
            map.setZoom($scope.mapZoom);
            map.setCenter(lastPoint);
            
            //Setting first point
            var image = baseUrl + '/assets/irisgps/icons/agents/firstPoint.png';
            var firstMarker = new google.maps.Marker({
                position: firstPoint,
                map: map,
                icon: image,
                scale: 10
            });

            if ($scope.animateFirstPoint) {
                firstMarker.setAnimation(google.maps.Animation.BOUNCE);
            }

            //Setting last point
            var image = baseUrl + '/assets/irisgps/icons/agents/lastPoint.png';
            var lastMarker = new google.maps.Marker({
                position: lastPoint,
                map: map,
                icon: image,
                scale: 10
            });

            if ($scope.animateLastPoint) {
                lastMarker.setAnimation(google.maps.Animation.BOUNCE);    
            }

            if (firstPoint && lastPoint) {
                console.log("centering according to firstMarker and lastMarker");
                console.log(firstPoint);
                console.log(lastPoint);
                var bound = new google.maps.LatLngBounds();
                bound.extend(new google.maps.LatLng(firstPoint.lat, firstPoint.lng));
                bound.extend(new google.maps.LatLng(lastPoint.lat, lastPoint.lng));
                map.setCenter(bound.getCenter());
            } 
        }
    }

    $scope.removePointsClose = function (items) {
        var copyOfitems = items.slice();
        var newItems = [];
        for (var i in copyOfitems) {
            if (copyOfitems[i].length >= 3) {
                newItems[i] = [];
                var to = copyOfitems[i].length;
                newItems[i].push(copyOfitems[i][0]);
                for (var j = 1 ; j < to; j++) {
                    var isAcceptable = true;
                    for (var k = 1 ; k < to; k++) {
                        var a = new google.maps.LatLng(copyOfitems[i][k].lat, copyOfitems[i][k].lng);
                        var b = new google.maps.LatLng(copyOfitems[i][j].lat, copyOfitems[i][j].lng);
                        var distance = google.maps.geometry.spherical.computeDistanceBetween(a, b);
                        if (distance > 0 && distance < $scope.radiousLimit) {
                            isAcceptable = false;
                            break;
                            //copyOfitems[i][j] = copyOfitems[i][k];
                        }
                    }
                    if (isAcceptable) {
                        newItems[i].push(copyOfitems[i][j]);
                    }
                }   
            }
        }

        return newItems;
    }
    $scope.drawPathUsingLines = function (items) {
        for(var i in items) {
            var lineSymbol = {
              path: 'M 0,-0.2 0,0.2',
              strokeOpacity: $scope.strokeAlpha,
              scale: $scope.strokeScale
            };

            var flightPlanCoordinates = items[i];

            var flightPath = new google.maps.Polyline({
                path: flightPlanCoordinates,
                geodesic: true,
                strokeColor: $scope.strokeColor,
                strokeOpacity: $scope.strokeAlpha / 4,
                strokeWeight: 6,
                icons: [{
                    icon: lineSymbol,
                    offset: '0',
                    repeat: $scope.strokeRepeat
                }]
            });

            flightPath.setMap(map);
        }
    }

    $scope.drawMarkers = function (items) {

        var iconUrl = baseUrl + '/assets/irisgps/icons/agents/disconnected.png';
        iconUrl = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 4,
            fillColor: 'yellow',
            strokeColor: $scope.strokeColor,
            strokeOpacity: $scope.strokeOpacityOfMarkers,
            strokeWeight: 10
        };

        if (items) {
            for(var i in items) {
                for (var j in items[i]) {
                    //if (!$scope.areTherePoint(items[i][j])) {
                    if (true) {
                        var title = "Hora: " + items[i][j].created_at;
                        if (items[i][j].speed) {
                            title += " Velocidad " + items[i][j].speed.toFixed(2) + ' Km/h';
                        }

                        if (items[i][j].id) {
                            title += " Code (" + items[i][j].id + ')';
                        }

                        if (items[i][j].distance) {
                            title += " Distancia (" + items[i][j].distance + ')';
                        }
                        
                        var marker = new google.maps.Marker({
                            position: items[i][j],
                            map: map,
                            title: title,
                            icon: iconUrl
                        });
                    }
                }
            }  
        }
    }

    $scope.areTherePoint = function (object) {
        var items = $scope.items;
        for(var i in items) {
                for (var j in items[i]) {
                    var a = new google.maps.LatLng(items[i][j].lat, items[i][j].lng);
                    var b = new google.maps.LatLng(object.lat, object.lng);
                    var distance = google.maps.geometry.spherical.computeDistanceBetween(a, b);
                    if (distance > 0 && distance < $scope.radiousLimit) {
                        return true;
                    }
                }
        }
        return false;
    }

    $scope.drawMap = function () {

    }

    $scope.areThereData = function () {
        return ($scope.items && $scope.items.length > 0);
    }

    $scope.init = function () {
        $scope.createMap();
        $scope.loadItems();
    }

    $scope.$watch('typeOfGeolocationHistory', function (value) {
        $scope.loadItems();
    });

    $scope.$watch('showMarkers', function (value) {
        $scope.loadItems();
    });

    $scope.$watch('strokeColor', function (value) {
        $scope.loadItems();     
    });

    $scope.$watch('strokeAlpha', function (value) {
        $scope.loadItems();  
    });

    $scope.$watch('radiousLimit', function (value) {
        $scope.loadItems();  
    });

    $scope.$watch('maxZoomLevel', function (value) {
        if (value) {
            $scope.maxZoomLevel = parseInt(value);
        }

        $scope.loadItems();  
    });

    $scope.$watch('showPathUsingLines', function (value) {
        $scope.loadItems();
    })
}]);

$(function () {
    $('#start_date').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
    $('#end_date').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
    $("#start_date").on("dp.change", function (e) {
    });
    $("#end_date").on("dp.change", function (e) {
        $('#start_date').data("DateTimePicker").maxDate(e.date);
    });
});