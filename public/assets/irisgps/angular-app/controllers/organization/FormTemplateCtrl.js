var irisGpsApp = angular.module('irisGpsApp', []);

irisGpsApp.controller('FormTemplateCtrl', ['$scope', '$http', 'FormTemplateService', function ($scope, $http, FormTemplateService) {

    $scope.item = {};
    $scope.items = {};
    $scope.paginator = {};

    $scope.save = function () {
        if (!$scope.item.id) {
            FormTemplateService.create($scope.item).then(function (response) {
                $scope.setNewItem();
                console.log($scope.item);
                displayToastr('toast-bottom-right', 'success', 'Se creó formulario exitosamente.');
            });
        } else {
            FormTemplateService.update($scope.item). then(function (response) {
                displayToastr('toast-bottom-right', 'success', 'Se actualizó formulario exitosamente.'); 
            });
        }
    }

    $scope.loadItems = function () {
        FormTemplateService.items({search: $scope.search}).then(function (response) {
            $scope.paginator = response;
            $scope.items = response.data;
        });
    }

    $scope.loadItem = function (itemId) {
        var options = {fields: 'formFields'};
        FormTemplateService.item(itemId, options).then(function (response) {
            $scope.item = dataToItem(response.data);
        });
    }

    $scope.destroy = function (itemId) {
        $.confirm({
            title: 'Confirmación',
            text: '¿Está seguro de eliminar esto?',
            confirm: function(){
                FormTemplateService.destroy(itemId).then(function () {
                    $scope.loadItems();
                });
            },
            cancel: function(){
                
            }
        });

    }

    $scope.addChildToItems = function () {
        $scope.item.formFields.items.push({ label: '' });
        
    }

    var dataToItem = function (data) {
        var newItem = {};
        if (typeof data === 'object') {
            for (var i in data) {
                
                if (i == 'form_fields') {
                    newItem['formFields'] = {
                        'force' : true,
                        'items': data[i]
                    };
                } else {
                    newItem[i] = data[i];
                }                
            }
        }

        return newItem;
    }

    $scope.removeItemFromFormFields = function (index) {
        $scope.item.formFields.items.splice(index, 1);
    }

    $scope.setNewItem = function () {
        $scope.item = {
            formFields: {
                force: true,
                items: [
                    {
                        label: ''
                    },
                    {
                        label: ''
                    },
                    {
                        label: ''
                    }
                ]
            }
        };
    }
    
    $scope.activateForm = function (formTemplateId) {

        FormTemplateService.activate(formTemplateId).then(function (response) {
            $scope.loadItems();
        })
    }
    $scope.setNewItem();
}]);