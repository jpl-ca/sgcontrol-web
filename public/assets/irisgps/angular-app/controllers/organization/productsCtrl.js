'use strict';
var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module('app', ['ngFileUpload']);


app.controller('ProductController', ['$scope', '$http', 'productsService', '$timeout', 'Upload', function($scope, $http, productsService, $timeout, Upload )
    {    	
        var html = function(id) { return document.getElementById(id); };
        $scope.redirect = function() {
          window.location=base_url+"/products";  
        }

        $scope.current_page = 1;
      
        $scope.$watch('current_page', function(value) {
           if (value >= 1) {
              $scope.getAll();
           };
        })

        $scope.page_anterior = function() {
          $scope.current_page = $scope.current_page - 1;
        };

        $scope.page_siguiente = function() {
          $scope.current_page = $scope.current_page + 1;
        };

        $scope.getAll = function() {
            productsService.getAll($scope.current_page).then(function (response) {
                $scope.products = response.data;
                $scope.current_page = response.current_page;
                console.log(response); 
                console.log($scope.current_page);                   
            }, function (response) {
                console.log(response);                
            });
        }
        // $scope.getAll();

        $scope.deleteProduct = function(id) {
		    if (confirm("¿Seguro que desea eliminar?") == true) {
		        productsService.deleteProduct(id).then(function (response) {
                $scope.getAll();                     
	            }, function (response) {
	                console.log(response);                
	            }); 
		    } else {
		         console.log(id);
		    }
        }


        $scope.visible = "all";
        $scope.$watch('searchType', function(value){
          if(value){
            if (value == 'name') {
                $scope.visible = "name";
            }else if (value == 'price') {
                $scope.visible = "price";
            }else if (value == 'category') {
                $scope.visible = "category";
            }else if (value == 'brand') {
                $scope.visible = "brand";
            }else if (value == 'all') {
                $scope.visible = "all";
            };
          }
         });
        $scope.searchProducts = function() {
            if ($scope.visible == "all") {
                $scope.getAll();
            }else{
                var searchTerm = html("searchTerm").value;
                console.log(searchTerm + ' ' + $scope.searchType);
                $scope.products = [];
                productsService.searchProducts(searchTerm, $scope.searchType).then(function (response) {
                    $scope.products = response.data;          
                }, function (response) {
                    console.log(response.data);                
                });
            };
        }

        $scope.addProduct = function() {
        	 productsService.addProduct($scope.product).then(function (response) {
                    $scope.product = "";
                    $timeout(function () {
	                	$scope.redirect();
	              		}, 500);                
                }, function (response) {
                    console.log(response.data);                
                });
        }      
        $scope.importcsv = function(file) {
        	 $scope.data = {
        	 archivo: file
        	 };
      	
        	// console.log(file);
        	console.log($scope.data);
        	 file.upload = Upload.upload({
              method: 'post',
              url: base_url + '/api/auth/organization/me/products/batch',
              data: $scope.data,
            });

            file.upload.then(function (response) {
               $scope.getAll();
              $scope.picFile = "";
              $scope.fileContent = [];
            }, function (response) {
              console.log(response);
            }, function (evt) {
            });
        }

    }]);

app.directive('fileReader', function() {
			return {
			    scope: {
			      fileReader:"="
			    },
			    link: function(scope, element) {
			      	$(element).on('change', function(changeEvent) {
				        var files = changeEvent.target.files;
				        if (files.length) {
				            var r = new FileReader();
				            r.onload = function(e) {
				                var contents = e.target.result;
				                scope.$apply(function () {
				                  

				                  var lines=contents.split("\n");

									  var result = [];
									  var separators = [' ', ','];
									  var headers=lines[0].split(new RegExp(separators.join('|')));
									  
									  for(var i=1;i<lines.length;i++){

										  var obj = {};
										  var currentline=lines[i].split(",");

										  for(var j=0;j<headers.length;j++){
											  obj[headers[j]] = currentline[j];
										  }
										  
										  result.push(obj);				  
									  }
									  console.log(headers);
									  scope.fileReader = result;
									  
				                });				                
				            };				          
				          r.readAsText(files[0]);
				        }				        
			      	});
			    }
			};
});


app.controller('ShowProductController', ['$scope', '$http', 'productsService', '$timeout', function($scope, $http, productsService, $timeout)
    {
        $scope.$watch('product_id', function(product_id){
          if(product_id){
            $scope.loadProduct(product_id);
          }
         });
        $scope.loadProduct = function(product_id) {
            productsService.getProduct(product_id).then(function (response) {
                $scope.product = response.data;        
                console.log($scope.product);            
            }, function (response) {
                console.log(response);                
            });
        }

        $scope.updateProduct = function() {
        	 productsService.updateProduct($scope.product_id,$scope.product).then(function (response) {
                    $timeout(function () {
	                	$scope.redirect();
	              		}, 500);                
                }, function (response) {
                    console.log(response.data);                
                });
        }

}]);

