'use strict';
var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module('app', []);



app.controller('ReportsController', ['$scope', '$http', 'reportsService', '$timeout', function($scope, $http, reportsService, $timeout)
{
        var html = function(id) { return document.getElementById(id); };

        $scope.getSales = function() {
              $scope.chart = "";
              reportsService.getSales(html("start_date").value, html("end_date").value)
                .then(function(data){ 
                    $scope.data = data.data;
        			$scope.chart = AmCharts.makeChart("chartdiv", {
                                    "type": "serial",
                                     "theme": "light",
                                    "categoryField": "first_name",                                    
                                    "rotate": true,
                                    "startDuration": 1,
                                    "categoryAxis": {
                                        "gridPosition": "start",
                                        "position": "left"
                                    },
                                    "trendLines": [],
                                    "graphs": [
                                        {
                                            "balloonText": "Total:S/.[[value]]",
                                            "fillAlphas": 0.8,
                                            "id": "AmGraph-1",
                                            "lineAlpha": 0.2,
                                            "title": "Total",
                                            "type": "column",
                                            "valueField": "total_sales"
                                        }
                                    ],
                                    "guides": [],
                                    "valueAxes": [
                                        {                                            
                                            "id": "ValueAxis-1",
                                            "position": "top",
                                            "axisAlpha": 0,
                                            "labelFunction": function(value) {
                                              return "S/." + value;
                                            }
                                        }
                                    ],
                                    "allLabels": [],
                                    "balloon": {},
                                    "titles": [],
                                    "dataProvider": $scope.data,
                                    "export": {
                                        "enabled": true,
                                        "position": "top-left", 
                                        "menu": [{
                                            "class": "export-main",
                                            "menu": [{
                                            "format": "XLSX",
                                            "label": "Exportar Excel",
                                            "title": "Exporta a Excel",
                                          },{
                                            "format": "PDF",
                                            "label": "Exportar PDF",
                                            "title": "Exporta a PDF",
                                          },{
                                            "format": "CSV",
                                            "label": "Exportar CSV",
                                            "title": "Exporta a CSV",
                                          },]
                                        }]
                                    } 
                  
                                });

           $scope.zoomChart = function(){
                                    $scope.chart.zoomToIndexes($scope.chart.dataProvider.length - 20, $scope.chart.dataProvider.length - 1);
                                }

                                $scope.chart.addListener("dataUpdated",   $scope.zoomChart);
                                $scope.zoomChart; 
            	}); 
        }


}]);




    $(function () {
            $('#start_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('#end_date').datetimepicker({
                    useCurrent: false,
                    format: 'YYYY-MM-DD'
            });
            $("#start_date").on("dp.change", function (e) {
                $('#end_date').data("DateTimePicker").minDate(e.date);
            });
            $("#end_date").on("dp.change", function (e) {
                $('#start_date').data("DateTimePicker").maxDate(e.date);
            });
    });
