function initMap(elementId, mapOptions) {
    var map;
    if (mapOptions == null) {
        mapOptions = {
            zoom: 8,
            center: {lat:-12.0879227, lng:-77.0159834},
            streetViewControl: false,
            mapTypeControl: false   ,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            geolocationNavigator: true
        }
    }
    var map = new google.maps.Map(document.getElementById(elementId), mapOptions);

    if (mapOptions.geolocationNavigator && mapOptions.geolocationNavigator == true) {
        navigator.geolocation.getCurrentPosition(function(position) {
            
                var geolocate = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
 
                map.setCenter(geolocate);
                
            });
    }

    return map;
    
}


function setMapStyles(map, styles) {
    map.setOptions({styles:styles});
}

function createMarker(latLng, title, icon, infoWindowContent) {
    if (title == null) {
        title = 'Marker';
    }

    var markerData = {
        position: latLng,
        title: title
    };

    if (icon != null) {
        markerData.icon = icon;
    }

    return marker = new google.maps.Marker(markerData);
}

function setMarkerOnMap(map, marker) {
    if (Array.isArray(marker)) {
        for (var i = 0; i < marker.length; i++) {
            marker[i].setMap(map);
        }
    }else {
        marker.setMap(map);
    }
}

function removeMarkerFromMap(marker) {
    setMarkerOnMap(null, marker);
}

function setInfoWindowOnMarker(map, marker, infoWindowContent) {
    if (infoWindowContent != null) {
        var infoWindow = new google.maps.InfoWindow({
            content: infoWindowContent,
            boxStyle: {
                padding: "0px"
            }
        });
        marker.addListener('click', function() {
            infoWindow.open(map, marker);
        });
    }
}

function setInfoBoxOnMarker(map, marker, infoBoxContent) {
    if (infoBoxContent != null) {
        var infobox = new InfoBox({
            content: infoBoxContent,
            disableAutoPan: false,
            maxWidth: 150,
            pixelOffset: new google.maps.Size(-140, 5),
            zIndex: null,
            boxStyle: {
                background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
                opacity: 0.8,
                width: "280px"
            },
            closeBoxMargin: "8px 8px 2px 2px",
            closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
            infoBoxClearance: new google.maps.Size(1, 1)
        });

        google.maps.event.addListener(marker, 'click', function() {
            smoothZoom(map, 17, map.getZoom());
            infobox.open(map, this);
            map.panTo(marker.position);
        });
    }
}

function smoothZoom (map, targetZoom, zoom) {
    if (zoom >= targetZoom) {
        return;
    }
    else {
        z = google.maps.event.addListener(map, 'zoom_changed', function(event){
            google.maps.event.removeListener(z);
            smoothZoom(map, targetZoom, zoom + 1);
        });
        setTimeout(function(){map.setZoom(zoom)}, 80);
    }
}

function loadMarkersFromHtml(map, infoBox) {
    var markersArray = [];

    if (infoBox == null) {
        infoBox = false;
    }

    var HTMLmarkers = $('.map-markers').find('div[data-role="marker"]');

    HTMLmarkers.each(function () {
        var lat = $(this).data("lat");
        var lng = $(this).data("lng");
        var title = $(this).data("title");
        var icon = iconBase() + $(this).data("icon");
        var infoWindow = $(this).data("info-window");

        var marker = createMarker({lat: lat, lng: lng}, title, icon);

        if (infoBox) {
            setInfoBoxOnMarker(map, marker, infoWindow);
        } else {
            setInfoWindowOnMarker(map, marker, infoWindow);
        }

        markersArray.push(marker);
    });

    return markersArray;
}