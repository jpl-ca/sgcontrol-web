<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

use Log;

class ProductOrder extends Model
{
    protected $columns = [
        'id',
        'description',
        'organization_id',
        'tasks_visit_point_id',
        'created_at',
        'updated_at',
        'status',
        'trackable_id',
        'trackable_type',
        'trackable_fullname',
    ];

	protected $fillable = [
        'name',
        'description',
        'category',
        'brand',
        'model',
        'price',
        'status',
        'state_id',
	];

    protected $appends = [
        'state_id',
        'currency_symbol',
    ];

	public function productOrderItems()
	{
		return $this->hasMany(ProductOrderItem::class);
	}

    public function visitPoint()
    {
            return $this->belongsTo(TasksVisitPoint::class, 'tasks_visit_point_id', 'id');
    }

    public function taskable()
    {
            return $this->morphTo();
    }

    public function setStateIdAttribute($value)
    {
        if (ProductOrderType::existsId($value)) {
            $this->attributes['status'] = ProductOrderType::getNameFromId($value);    
        }
        
    }

    public function getStateIdAttribute()
    {
        $value = ProductOrderType::getIdFromName($this->status);
        return $value;
    }

    public function getCurrencySymbolAttribute()
    {
        $currency = Currency::find($this->currency_id);

        if (!isset($currency)) {
            return null;
        }

        return $currency->symbol;
    }
}
