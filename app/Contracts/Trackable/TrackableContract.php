<?php

namespace IrisGPS\Contracts\Trackable;


interface TrackableContract
{
    public function organization();

    public function trackable();

    public function getTokenAttribute();

    public function getTypeAttribute();
}