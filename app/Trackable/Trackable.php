<?php


namespace IrisGPS\Trackable;

use IrisGPS\Organization;
use IrisGPS\Task;
use IrisGPS\Trackable as TrackableModel;


trait Trackable
{
    /**
     * The trackable object type.
     *
     * @var string
     */
    protected $trackableType;

    /**
     * Initialize trackable default variables.
     */
    protected function initialize()
    {
        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        $this->hidden = [
            'password',
            'trackable',
            'organization'
        ];

        /**
         * The accessors to append to the model's array form.
         *
         * @var array
         */
        $this->appends = ['token', 'type', 'location_frequency'];
    }

    /**
     * Get the organization where this trackable belongs.
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }

    /**
     * Get the trackable device.
     */
    public function trackable()
    {
        return $this->morphOne(TrackableModel::class, 'trackable');
    }

    /**
     * Get all the trackable's tasks.
     */
    public function tasks()
    {
        return $this->morphMany(Task::class, 'taskable');
    }

    public function getTokenAttribute()
    {
        return $this->trackable->api_token;
    }

    public function getTypeAttribute()
    {
        return $this->trackableType;
    }

    public function getLocationFrequencyAttribute()
    {
        $frequencyType = $this->trackableType . 's_location_frequency';
        return $this->organization->$frequencyType;
    }
}