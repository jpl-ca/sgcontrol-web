<?php

namespace IrisGPS;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use DB;

class Organization extends Model
{
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'ruc',
        'country_suffix',
        'country',
        'document_type',
        'document_number',
        'phone_number',
        'category'
    ];

    public function subscriptionPlan()
    {
        return \Subscriber::plans()->find($this->subscription_id);
    }

    public function isCurrentPlanExpired()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->subscription_ends)->isPast();
    }

    public function users()
    {
        return $this->hasMany(User::class, 'organization_id', 'id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class, 'organization_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(OrganizationPayment::class, 'organization_id', 'id');
    }

    public function planExpiredDays()
    {
        if ($this->isCurrentPlanExpired()) {
            $expirationDate = Carbon::createFromFormat('Y-m-d H:i:s', $this->subscription_ends);
            $now = Carbon::now()->endOfDay();
            return $now->diffInDays($expirationDate);
        }
        return 0;
    }

    public function isInGracePeriod()
    {
        return $this->subscriptionRemainingGraceDays() > 0;
    }

    public function isGracePeriodExpired()
    {
        return $this->subscriptionRemainingGraceDays() <= 0;
    }

    public function subscriptionRemainingGraceDays()
    {
        return 15 - $this->planExpiredDays();
    }

    public function agents()
    {
        return Agent::query()->where('organization_id', $this->id);
    }

    public function vehicles()
    {
        return Vehicle::query()->where('organization_id', $this->id);        
    }

    public function visitPoints()
    {
        return $this->hasManyThrough(TasksVisitPoint::class, Task::class);
    }

    public static function itemsCreatedGroupedByDay($options = [])
    {
        $now = Carbon::now();
        $sqlParams = [];
        $endDate = $now->toDateTimeString();
        $startDate = $now->addMonths(-2)->toDateTimeString();

        if (isset($options['startDate'])) {
            $startDate = $options['startDate'];
        }

        if (isset($options['endDate'])) {
            $endDate = $options['endDate'];
            $endDate = Carbon::parse($options['endDate'])->addDays(1);
        }

        // Building query
        $query = "
        SELECT
            date(organizations.created_at) date,
            count(organizations.id) total
        FROM 
            organizations
        WHERE 
            1=1
            AND organizations.created_at >= :startDate
            AND organizations.created_at <= :endDate
        GROUP BY
            organizations.created_at
        ORDER BY
            organizations.created_at desc
        ";

        if (isset($startDate)) $sqlParams['startDate'] = $startDate;
        if (isset($endDate)) $sqlParams['endDate'] = $endDate;

        $rows = DB::select($query, $sqlParams);

        return $rows;
    }

    public static function listOfAgentsCreated($options)
    {
        $sqlParams = [];

        $startDate = isset($options['startDate'])
            ? $options['startDate']
            : Carbon::now()->addMonths(-3)->toDateTimeString();

        $endDate = isset($options['endDate'])
            ? $options['endDate']
            : Carbon::now()->toDateTimeString();

        $query = "
            select 
                organizations.name,
                count(agents.organization_id) total
            from organizations
            left join agents
                on(agents.organization_id = organizations.id)
            where
                1=1
                AND agents.created_at >= :startDate
                AND agents.created_at <= :endDate 
                OR agents.created_at is null
            group by
                organizations.id
            order by
                organizations.created_at
        ";
        if ($startDate) $sqlParams['startDate'] = $startDate;
        if ($endDate) $sqlParams['endDate'] = $endDate;

        $rows = DB::select($query, $sqlParams);

        return $rows;
    }

    public function tasksVisitPoints()
    {
        return $this->hasManyThrough(TasksVisitPoint::class, Task::class);
    }

    public function markers()
    {
        return $this->hasMany(Marker::class);
    }
}
