<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

use IrisGPS\BaseApiModel as ModelTrait;

class Product extends Model
{
	protected $fillable = [
		'name',
		'price',
		'stock',
		'description',
		'category',
		'brand',
		'model',
		'organization_id',
		'currency_id',
	];

	protected $appends = [
		'brief_description',
		'currency_symbol',
	];

	public function getBriefDescriptionAttribute()
	{
		return str_limit($this->description, 28);
	}

	public function getCurrencySymbolAttribute()
	{
		$currency = Currency::find($this->currency_id);

		if (!isset($currency)) {
			return null;
		}

		return $currency->symbol;
	}
}
