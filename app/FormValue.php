<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class FormValue extends Model
{
    protected $fillable = [
    	'machine_name',
    	'value',
    	'label',
    	'form_id',
    	'type'
    ];
}
