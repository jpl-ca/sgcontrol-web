<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

use Log;

class ProductOrderType
{
    public static $pending = 1;
    public static $confirmed = 2;
    public static $done = 3;
    public static $canceled = 4;

    public static function names()
    {
        return array_keys(self::keysAndValues());
    }

    public static function values()
    {
        return array_values(self::keysAndValues());   
    }

    public static function keysAndValues()
    {
        return [
            'pending' => 1,
            'confirmed' => 2,
            'done' => 3,
            'canceled' => 4
        ];
    }

    public static function existsName($value)
    {
        $v = strtolower($value);
        return in_array($v, self::names());
    }

    public static function existsId($value)
    {
        $v = (int)($value);
        return in_array($v, self::values());
    }

    public static function getIdFromName($name)
    {
        foreach (self::keysAndValues() as $key => $val) {
            if ($key == $name) {
                return $val;
            }
        }
        return null;
    }

    public static function getNameFromId($value)
    {
        $id = (int) $value;
        foreach (self::keysAndValues() as $key => $val) {
            if ($val == $id) {
                return $key;
            }
        }
        return null;
    }
}
