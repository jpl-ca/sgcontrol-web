<?php

namespace IrisGPS;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Trackable extends Model
{
    const STATE_ACTIVE = 'active';
    const STATE_INACTIVE = 'inactive';
    const STATE_DISCONNECTED = 'disconnected';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'api_token',
        'notification_id',
    ];

    protected $hidden = [
      'trackable'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['state', 'type', 'last_lat', 'last_lng', 'full_name', 'icon', 'info_window'];

    /**
     * Get all of the owning likeable models.
     */
    public function trackable()
    {
        return $this->morphTo();
    }

    public function geolocationHistory()
    {
        return $this->hasMany(GeolocationHistory::class, 'trackable_id', 'id');
    }

    public function getStateAttribute()
    {
        if ($this->isConnected()) {
            $startDate = $this->lastGeolocationDateTime();
            $addSeconds = 60 * 10;
            $endDate = $startDate->copy()->addSeconds($addSeconds);

            if ($endDate->isPast()) {
                return self::STATE_INACTIVE;
            }
            return self::STATE_ACTIVE;
        }
        return self::STATE_DISCONNECTED;
    }

    public function regenerateToken()
    {
        $this->api_token = str_random(60);
        $this->save();
    }

    public function removeToken()
    {
        $this->api_token = null;
        $this->notification_id = null;
        $this->save();
    }

    public function updateNotificationId($notificationId)
    {
        $this->notification_id = $notificationId;
        $this->save();
    }

    public function isConnected()
    {
        return !is_null($this->api_token);
    }

    public function hasGeolocation(Carbon $startDate = null, Carbon $endDate = null)
    {
        if (is_null($startDate)) {
            return $this->geolocationHistory()
                ->get()->count() > 0;
        } elseif(is_null($startDate)) {
            return $this->geolocationHistory()
                ->where('created_at', '>=', $startDate->toDateTimeString())
                ->get()->count() > 0;
        } else {
            return $this->geolocationHistory()
                ->where('created_at', '>=', $startDate->toDateTimeString())
                ->where('created_at', '<=', $endDate->toDateTimeString())
                ->get()->count() > 0;
        }

    }

    public function hasPath(Carbon $startDate = null, Carbon $endDate = null)
    {
        if (is_null($startDate)) {
            return $this->geolocationHistory()
                ->get()->count() > 1;
        } elseif(is_null($startDate)) {
            return $this->geolocationHistory()
                ->where('created_at', '>=', $startDate->toDateTimeString())
                ->get()->count() > 1;
        } else {
            return $this->geolocationHistory()
                ->where('created_at', '>=', $startDate->toDateTimeString())
                ->where('created_at', '<=', $endDate->toDateTimeString())
                ->get()->count() > 1;
        }

    }

    public function lastGeolocation()
    {
        return $this->geolocationHistory()->orderBy('created_at', 'desc')->first();
    }

    public function lastGeolocationDateTime()
    {
        if ($this->hasGeolocation()) {
            $dateTime = $this->lastGeolocation()->created_at;
        } else {
            $dateTime = new Carbon;
            $dateTime = $dateTime->year(1800);
        }
        return $dateTime;
    }

    public function transmissionFrequency()
    {
        $frequencyColumn = $this->trackable->type . 's_location_frequency';
        return $this->trackable->organization->$frequencyColumn;
    }

    public function type()
    {
        return $this->trackable->type;
    }

    public function getTypeAttribute()
    {
        return $this->type();
    }

    public function getLastLatAttribute()
    {
        return $this->lastGeolocation()->lat;
    }

    public function getLastLngAttribute()
    {
        return $this->lastGeolocation()->lng;
    }

    public function getFullNameAttribute()
    {
        return $this->trackable->full_name;
    }

    public function getIconAttribute()
    {
        return $this->getIcon();
    }

    public function getInfoWindowAttribute()
    {
        return $this->getInfoWindow();
    }

    public function isAgent()
    {
        return get_class($this->trackable) == Agent::class;
    }

    public function isVehicle()
    {
        return get_class($this->trackable) == Vehicle::class;
    }

    public function getIcon()
    {
        switch(get_class($this->trackable)) {
            case Vehicle::class:
                return 'vehicles/' . $this->state . '.png';
            case Agent::class:
                return 'agents/' . $this->state . '.png';
        }
    }

    public function getInfoWindow()
    {
        if ($this->isAgent()) {
            $view = view('web.trackables.agents.partials.info-window')->with('agent', $this->trackable)->render();
        } elseif ($this->isVehicle()) {
            $view = view('web.trackables.vehicles.partials.info-window')->with('vehicle', $this->trackable)->render();
        }

        return $view;
    }

    public function visitPointsGroupedByDayAndState($options = [])
    {

    }

    public function sendPushNotification($data)
    {
        $pusher = new Pusher;
        return $pusher->send([
            'receiver' => '/topics/trackables_' . $this->id,
            'data' => $data
        ]);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
