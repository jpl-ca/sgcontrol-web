<?php

namespace IrisGPS\Http\Controllers\Api;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\TasksVisitPoint;

use IrisGPS\VisitState;

class AuthenticatedUserOrganizationVisitPoint extends Controller
{
    public function rescheduled(Request $request)
    {
    	$user = auth()->user();
    	$visits = TasksVisitPoint::query();
    	$visits->join('tasks', 'tasks.id', '=', 'tasks_visit_points.task_id');
    	$visits->where('tasks.organization_id', $user->organization_id);
    	$visits = $visits->where('visit_state_id', VisitState::STATE_RESCHEDULING_REQUEST);

    	$visits = $visits->paginate();

    	return response()->json($visits);
    }

    public function history(Request $request, $visitPointId)
    {
        $visitPoint = TasksVisitPoint::findOrFail($visitPointId);
        $json = [
            'message' => 'Historial cargado exitosamente',
            'data' => $visitPoint->children()
        ];

        return response()->json($json);
    }
}
