<?php

namespace IrisGPS\Http\Controllers\Api\Organization\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Controllers\BaseApiController;

use IrisGPS\TasksVisitPoint;

use Maatwebsite\Excel\Facades\Excel;

class VisitPointController extends Controller
{
    use BaseApiController;

    public function __construct()
    {
        $this->classReference = TasksVisitPoint::class;
    }

    public function itemsGroupedByDay(Request $request)
    {
        $user = auth()->user();
        $format = isset($request->format) ? $request->format : 'json';

        $items = TasksVisitPoint::itemsGroupedByDay(
            [
                'startDate' => $request->startDate,
                'endDate' => $request->endDate,
                'organizationId' => $user->organization_id
            ]
        );

        switch ($format) {
            case 'xls':

                Excel::create('Puntos de visita', function($excel) use ($items) {
                    $excel->sheet('', function($sheet) use ($items) {
                        $sheet->row(1,
                            [
                                'Fecha',
                                'Programados',
                                'Terminados',
                                'Reprogramador',
                                'Cancelados'
                            ]);

                        $i = 2;

                        foreach ($items as $item) {
                            $sheet->row(
                                $i,
                                [
                                    $item->date,
                                    $item->scheduled,
                                    $item->done,
                                    $item->rescheduled,
                                    $item->cancelled
                                ]
                            );

                            $i++;
                        }
                    });

                })->download();

                break;
            case 'json':
                    
            default:
                $json = $items;
                return response()->json($json);     
                break;
        }
        
    }
}
