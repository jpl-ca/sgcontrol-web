<?php

namespace IrisGPS\Http\Controllers\Api\Organization\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Agent;

use Maatwebsite\Excel\Facades\Excel;

class AgentController extends Controller
{
    public function viewVisitPointsByDay(Request $request, $agentId)
    {

    	$startDate = $request->has('startDate') ? $request->startDate : null;
    	$endDate = $request->has('endDate') ? $request->endDate : null;
    	$statuses = $request->has('statuses') ? $request->statuses : null;
    	$format = $request->has('format') ? $request->format : 'json';
    	
    	$agent = Agent::find($agentId);

        // Building response
    	switch ($format) {
            // JSON Format
    		case 'json':
                $json = [
                    'message' => 'Lista de puntos de visita cargados exitosamente',
                    'data' => null
                ];

                if (!$agent) {
                    $json['message'] = 'Agente no existe';
                } else {
                    $json['data'] = $agent->listVisitPointsByDay([
                        'startDate' => $startDate,
                        'endDate' => $endDate,
                        'statuses' => $statuses
                    ]);
                }

		    	return response()->json($json);

    			break;

            // Spreadsheet Format
            case 'xls':
                $visitPoints = $agent->listVisitPointsByDay([
                        'startDate' => $startDate,
                        'endDate' => $endDate,
                        'statuses' => $statuses
                ]);

                //Creating excel file
                Excel::create('visitPoints', function ($excel) use ($visitPoints) {
                    $excel->sheet('Puntos de visita', function ($sheet) use ($visitPoints) {
                        // Add header columns
                        $sheet->row(1, [
                            'Nombre',
                            'Teléfono',
                            'Direción',
                            'Referencia'
                        ]);

                        $i = 2;
                        foreach ($visitPoints['days'] as $key => $value) {
                            foreach ($visitPoints['days'][$key] as $item) {
                                $sheet->row($i, [
                                    $item->name,
                                    $item->phone,
                                    $item->address,
                                    $item->reference
                                ]);

                                $i++;
                            }
                        }
                    });

                })->download();

                break;
    	}
    }

    public function geolocationHistory(Request $request, $agentId)
    {
        $agent = Agent::find($agentId);

        $options['startDate'] = $request->startDate;
        $options['endDate'] = $request->endDate;


        if ($request->exists('type')) {
            $type = $request->type;
        } else {
            $type = 'snapped';
        }

        $json = [];
        // Clearing noise
        if ($type == 'clean') {
            $geolocationHistory = $agent->cleanGeolocationHistorySessions($options);
            $json = [
                'message' => 'Lista(' . $type . ') de ubicaciones cargadas exitosamente. Desde ' . $options['startDate'] . ' hasta ' . $options['endDate'],
                'data' => $geolocationHistory
            ];
        }  elseif ($type == 'raw') {
            $geolocationHistory = $agent->geolocationHistorySessions($options);
            $json = [
                'message' => 'Lista(' . $type . ') de ubicaciones cargadas exitosamente. Desde ' . $options['startDate'] . ' hasta ' . $options['endDate'],
                'data' => $geolocationHistory
            ];
        } elseif ($type == 'snapped') {
            $geolocationHistory = $agent->snappedGeolocationHistorySessions($options);
            $json = [
                'message' => 'Lista(' . $type . ') de ubicaciones cargadas exitosamente. Desde ' . $options['startDate'] . ' hasta ' . $options['endDate'],
                'data' => $geolocationHistory
            ];
        }

        // End
        return response()->json($json);
    }
}
