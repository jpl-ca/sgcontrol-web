<?php

namespace IrisGPS\Http\Controllers\Api\Organization\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Controllers\BaseApiController;

use IrisGPS\Form;

class FormController extends Controller
{
    use BaseApiController;

    public function __construct()
    {
    	$this->classReference = Form::class;
    }
}
