<?php

namespace IrisGPS\Http\Controllers\Api\Organization\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Controllers\BaseApiController;

use IrisGPS\ProductOrder;

use IrisGPS\BaseApiModel;

class ProductOrderController extends Controller
{
	use BaseApiController {
		create as createTrait;
		#index as indexTrait;
	}

	public function __construct()
	{
		$this->classReference = ProductOrder::class;
	}

	public function create(Request $request, $productId)
	{
		$request->merge(['product_id']);
		$this->createTrait($request);
	}

	public function index(Request $request)
	{
		$merge = [
			'filters' => [
				[
					'field' => 'organization_id',
					'value' => auth()->user()->organization_id
				]
			]
		];

		$request->merge($merge);

		// Setting initial variables
        $classReference = $this->classReference;
        $format = $request->format ? $request->format : 'json';
        $options = [];
    	$options['page'] = $request->page ? $request->page : 1;
        $options['startDate'] = $request->startDate ? $request->startDate : null;
        $options['endDate'] = $request->endDate ? $request->endDate : null;
    	$options['searchTerm'] = $request->searchTerm ? $request->searchTerm : null;
        $options['searchType'] = $request->searchType ? $request->searchType : null;
        $options['filters'] = $request->filters ? $request->filters : null;

        if (isset($request->includes)) {
        	$list = explode(',', $request->includes);
        	$options['includes'] = $list;
        }
        
        $items = BaseApiModel::searchItemsQuery($classReference, $options);
        $items->with('visitPoint');

        $items = $items->paginate();

        // Building Response
        switch ($format) {
            case 'xls':
                return $this->buildXlsFile($items)->download();

            case 'json':
            default:
                //Building JSON
                $json = $items;

                return response()->json($json);
                break;    
        }
	}
}
