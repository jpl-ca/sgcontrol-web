<?php

namespace IrisGPS\Http\Controllers\Api\Organization\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Controllers\BaseApiController;

use IrisGPS\Product;

use IrisGPS\Currency;

use Maatwebsite\Excel\Facades\Excel;

use Validator;

class ProductController extends Controller
{
   use BaseApiController {
   	create as traitCreate;
      index as traintIndex;
   }
   
   public function __construct()
   {
   	$this->classReference = Product::class;
   }

   public function create(Request $request)
   {
   	$organizationId = auth()->user()->organization_id;
      $organization = auth()->user()->organization;
   	$request->merge(['organization_id' => $organizationId]);

      if (isset($organization->country_suffix)) {
         $currency = Currency::where('iso_country', $organization->country_suffix)->first();
         if ($currency) $request->merge(['currency_id' => $currency->id]);
      }

   	return $this->traitCreate($request);
   }

   public function index(Request $request)
   {

      $filters = $request->filters;

      $organizationId = $organizationId = auth()->user()->organization_id;

      if (isset($filters) && is_array($filters)) {
         $filters[] = [
            'field' => 'organization_id',
            'value' => $organizationId
         ];

      } else {
         $filters[] = [
            'field' => 'organization_id',
            'value' => $organizationId
         ];
      }

      $request->merge(['filters' => $filters]);

      return $this->traintIndex($request);
   }

   public function saveBatch(Request $request)
   {
      $organization = auth()->user()->organization;

      $currency = null;
      if (isset($organization->country_suffix)) {
         $currency = Currency::where('iso_country', $organization->country_suffix)->first();
      }

      if (!$request->hasFile('archivo')) {
         $json = [
            'message' => 'errores',
            'errors' => [
               'file' => 'El archivo a procesar no está presente'
            ]
         ];

         return response()->json($json, 422);
      }

      $target = storage_path('app/tmp/');
      $filename = rand(1,9999999);

      $request->file('archivo')->move($target, $filename);

      $url = $target . $filename;

      Excel::load($url, function($reader) use ($currency){
 
         foreach ($reader->get() as $key => $item) {

            $newProduct = new Product;
            $newProduct->organization_id = auth()->user()->organization_id;

            if (isset($item->name)) $newProduct->name = $item->name;
            if (isset($item->description)) $newProduct->description = $item->description;
            if (isset($item->brand)) $newProduct->brand = $item->brand;
            if (isset($item->model)) $newProduct->model = $item->model;
            if (isset($item->price)) $newProduct->price = $item->price;
            if (isset($item->category)) $newProduct->category = $item->category;
            if (isset($item->stock)) $newProduct->stock = $item->stock;
            if (isset($currency)) $newProduct->currency_id = $currency->id;
            $newProduct->save();

         }
      });

      $json = [
         'message' => 'Productos importados exitosamente',
      ];

      return response()->json($json);
   }
}
