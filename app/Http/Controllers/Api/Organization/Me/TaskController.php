<?php

namespace IrisGPS\Http\Controllers\Api\Organization\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Controllers\BaseApiController;

use IrisGPS\Task;

use Carbon\Carbon;

class TaskController extends Controller
{
    use BaseApiController;

    public function __construct()
    {
        $this->classReference = Task::class;
    }

    public function calendar(Request $request)
    {
        // Settings initial variables
        $agents = [];
        $visit_points = [];

        // Fetching agents
        $items = $this->getCurrentOrganization()
                      ->agents()
                      ->get();

        // Building json format of agents
        foreach ($items as $item) {
            
            $agents[] = [
                'key' => 'IrisGPS\Agent-' . $item->id,
                'label' => $item->full_name
            ];
        }

        // Fetching visit points
        $items = $this->getCurrentOrganization()
                      ->visitPoints();

        if (isset($request->taskable_id)) {
            $items = $items->where('tasks.taskable_id', $request->taskable_id);
            $items = $items->where('tasks.taskable_type', 'IrisGPS\Agent');
        }

        if (isset($request->startDate) ) {
            $startDate = Carbon::parse($request->startDate)->startOfMonth()->toDateString();
            $items = $items->where('tasks_visit_points.created_at','>=', $startDate);

            $endDate = Carbon::parse($request->endDate)->endOfMonth()->toDateString();
            $items = $items->where('tasks_visit_points.created_at','<=', $endDate);
        }
        
        $items = $items->get();

        if (count($items) > 0) {

            foreach ($items as $item) {

                $task = $item->task;
                $pseudoIdOfTrackable = $task->taskable_type . '-' . $task->taskable_id;
                $agent_id = $task->taskable_id;
                $time = isset($item->datetime) ? Carbon::parse($item->datetime)->format('H:i') : null;
                $item->load('productOrder.productOrderItems');
                $item->load('state');

                $visit_points[] = [
                    'key' => $item->id,
                    'start_date' => Carbon::parse($item->datetime)->toDateTimeString(),
                    'end_date' => Carbon::parse($item->datetime)->toDateTimeString(),
                    'name' => $item->name,
                    'text' => $time . ' - ' . $item->name,
                    'lat' => $item->lat,
                    'lng' => $item->lng,
                    'checklist' => null,
                    'product_order' => $item->productOrder,
                    'trackable_id' => $pseudoIdOfTrackable,
                    'task_id' => $task->id,
                    'time' => $time,
                    'state' => $item->state,
                    'marker_id' => $item->marker_id,
                    'datetime' => $item->datetime,
                    'color' => "#" . $item->getHexadecimalColorAttribute()
                ];
            }    
        }
        



        // Building json format
        $json = [
            'message' => 'Calendario cargado exitosamente',
            'data' => [
                'trackables' => $agents,
                'visit_points' => $visit_points
            ]
        ];

        return response()->json($json);
    }

    private function getCurrentOrganization()
    {
        return auth()->user()->organization;
    }
}
