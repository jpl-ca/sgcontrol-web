<?php

namespace IrisGPS\Http\Controllers\Api\Organization\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;
use IrisGPS\TasksVisitPoint;

use Maatwebsite\Excel\Facades\Excel;

class TrackableController extends Controller
{
    public function visitPointsReport(
	    	Request $request,
	    	$trackableType,
	    	$trackableId
	)
    {

    	$format = isset($request->format) ? $request->format : 'json';

    	$items = TasksVisitPoint::itemsGroupedByDay(
    		[
    			'startDate' => $request->startDate,
    			'endDate' => $request->endDate,
    			'trackableType' => $trackableType,
    			'trackableId' => $trackableId
    		]
    	);

    	switch ($format) {
    		case 'xls':

    			Excel::create('Puntos de visita', function($excel) use ($items) {
                    $excel->sheet('', function($sheet) use ($items) {
                        $sheet->row(1,
                        	[
	                            'Fecha',
	                            'Programados',
	                            'Terminados',
	                            'Reprogramados',
	                            'Cancelados'
                            ]);

                        $i = 2;

                        foreach ($items as $item) {
                            $sheet->row(
                            	$i,
                            	[
	                                $item->date,
	                                $item->scheduled,
	                                $item->done,
	                                $item->rescheduled,
	                                $item->cancelled
                            	]
                            );

                            $i++;
                        }
                    });

                })->download();

    			break;
    		case 'json':
    				
    		default:
    			$json = [];
    			$json['data'] = $items;
    			return response()->json($json);		
    			break;
    	}
    	
    }
}
