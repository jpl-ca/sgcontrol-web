<?php

namespace IrisGPS\Http\Controllers\Api\Organization\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Controllers\BaseApiController;

use IrisGPS\FormTemplate;

use IrisGPS\FormTemplateState;

class FormTemplateController extends Controller
{
	use BaseApiController {
		create as createTrait;
	}

	public function __construct()
	{
		$this->classReference = FormTemplate::class;
	}

	public function create(Request $request)
	{
		$user = $this->getUser();
		$request->merge(['organization_id' => $user->organization_id]);
		return $this->createTrait($request);
	}

	public function getUser()
	{
		return auth()->user();
	}

	public function activate(Request $request, $id)
	{
		$user = auth()->user();
		$formTemplate = FormTemplate::where('organization_id', $user->organization_id)
			->where('id', $id)->first();

		if (!$formTemplate) {
			$json = [
				'message' => 'Usted no está autorizado para activar este formulario'
			];

			return response()->json($json, 422);
		}

		FormTemplate::where('organization_id', $user->organization_id)->update(['state' => FormTemplateState::$inactive]);

		$formTemplate->state = FormTemplateState::$active;
		$formTemplate->save();

		$json = [
			'message' => 'Se activado el formulario eexitosamente',
			'data' => $formTemplate
		];

		return response()->json($json);
	}
}
