<?php

namespace IrisGPS\Http\Controllers\Api;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;
use IrisGPS\Vehicle;

class VehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('auth:web');
    }

    public function index()
    {
        $agents = auth()->user()->organization->vehicles()->get();
        return response()->json($agents);
    }
}
