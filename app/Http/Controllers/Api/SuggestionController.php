<?php

namespace IrisGPS\Http\Controllers\Api;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Suggestion;

use Validator;

use Symfony\Component\HttpFoundation\Response as HttpResponse;

class SuggestionController extends Controller
{
	public function __contruct()
	{
		$this->middleware('web');
        $this->middleware('auth:web');
	}

	public function store(Request $request)
	{
		//return response()->json(auth('api')->check());
		// Setting initial vars
		$organization_id = $request->exists('organization_id') ? $request->organization_id : null;
		$user_id = $request->exists('user_id') ? $request->user_id : null;

		//Validating inputs
		$validator = Validator::make($request->all(), [
			'content' => 'required'
		]);

		if ($validator->fails()) {
			
			$json = [
				'message' => 'Errors de entrada',
				'errors' => $validator->errors()
			];

			return response()->json(
					$json,
					HttpResponse::HTTP_UNPROCESSABLE_ENTITY
				);
		}

		// Saving suggestions
		$suggestion = new Suggestion;
		$suggestion->content = $request->content;

		if (isset($organization_id)) $suggestion->organization_id = $organization_id;
		if (isset($user_id)) $suggestion->user_id = $user_id;

		$suggestion->save();

		//Building json
		$json = [
			'message' => 'Sugerencia registrada exitosamente',			
		];
		
		return response()->json($json);
	}
}
