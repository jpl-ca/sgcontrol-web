<?php

namespace IrisGPS\Http\Controllers\Api;

use IrisGPS\Agent;
use IrisGPS\Http\Controllers\Controller;
use IrisGPS\Http\Requests;
use Illuminate\Http\Request;
use IrisGPS\Organization;
use IrisGPS\Vehicle;

class OrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => 'validateExistence']);
    }

    public function validateExistence(Request $request)
    {
        $item = Organization::where('name', $request->name)->first();

        $httpStatusCode = 200;

        if (!isset($item)) {
        	$json = [
        		'message' => 'Organización no encontrada',
        		'errors' => [
        			'name' => 'No válido'
        		]
        	];

        	$httpStatusCode = 422;
        } else {
        	$json = [
        	'message' => 'Organización válida',
        	'data' => $item
        	];	
        }

        return response()->json($json, $httpStatusCode);
    }

}