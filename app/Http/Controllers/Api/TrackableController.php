<?php

namespace IrisGPS\Http\Controllers\Api;

use Illuminate\Http\Request;

use IrisGPS\Agent;
use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;
use IrisGPS\Trackable;

class TrackableController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('auth:web');
    }

    public function disconnect(Request $request)
    {
        $trackable = Trackable::where('trackable_id', $request->id)
            ->where('trackable_type', $request->type)
            ->first();

        if (!is_null($trackable)) {
            $trackable->removeToken();
            return response()->json(ucfirst($trackable->trackable->type) . ' disconnected successfully!.');
        }

        return response()->json(ucfirst($trackable->trackable->type) . ' can not be disconnected!.', 422);
    }

    public function getWithGeolocation()
    {
        // TODO Mejorar este metodo, no es óptimo
        $organization = auth()->user()->organization;
        $items = [];
        //dd($organization->agents()->get());
        foreach ($organization->agents()->get() as $agent) {
            //dd("pasoo");
            
            if ($agent->trackable->hasGeolocation()) {
                $items[]  = $agent->trackable;
            }
        }

        foreach ($organization->vehicles()->get() as $vehicle) {
            //dd("pasoo");
            
            if ($vehicle->trackable->hasGeolocation()) {
                $items[]  = $vehicle->trackable;
            }
        }

        return response()->json($items);
    }
}
