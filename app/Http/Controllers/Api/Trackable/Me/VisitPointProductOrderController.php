<?php

namespace IrisGPS\Http\Controllers\Api\Trackable\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\ProductOrder;

use IrisGPS\ProductOrderItem;

use Validator;

class VisitPointProductOrderController extends Controller
{
    use GetUser;

    public function save(Request $request, $visitPointId)
    {

    	$organizationId = $this->getUser()->organization_id;

        $validator = Validator::make($request->all(), []);

        if ($validator->fails()) {
            $json = [
                'message' => 'Error al grabar pedido de productos',
                'errors' => $validator->errors()
            ];

            return response()->json($json);
        }

        // Saving data
        $item = ProductOrder::where('tasks_visit_point_id', $visitPointId)->first();

        if (!isset($item)) {
            $item = new ProductOrder;
        }

        $item->tasks_visit_point_id = $visitPointId;
    	$item->fill($request->all());
        $item->organization_id = $organizationId;

        if (!$item->isDirty('status') && $item->status == 'canceled') {
            $item->status = 'pending';
        }

        if ($item->status == 'confirmed' && ($item->productOrderItems()->count() == 0)) {

            $json = [
                'message' => 'No puedes confirmar un pedido con un lista vacía.'        
            ];

            return response()->json($json, 422);
        }

    	$item->save();
        
        $items = [];

        if (isset($request->productOrderItems)) {
            foreach ($request->productOrderItems as $i) {
                $items[] = new ProductOrderItem($i);
            }

            $item->productOrderItems()->delete();

            $item->productOrderItems()->saveMany($items);
        }

    	$json = [
    		'message' => 'Guardado exitosamente',
    		'data' => $item
    	];

    	return response()->json($json);
    }
}
