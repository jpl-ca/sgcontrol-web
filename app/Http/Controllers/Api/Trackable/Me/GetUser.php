<?php
namespace IrisGPS\Http\Controllers\Api\Trackable\me;

trait GetUser
{
	public function getUser()
	{
		return auth('api')->user()->trackable;
	}
}