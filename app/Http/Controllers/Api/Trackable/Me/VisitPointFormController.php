<?php

namespace IrisGPS\Http\Controllers\Api\Trackable\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Form;

use IrisGPS\FormTemplate;

use IrisGPS\FormValue;

use Validator;

class VisitPointFormController extends Controller
{
    public function save(Request $request, $visitPointId)
    {
        // Making validator
        $validator = Validator::make($request->all(), [
            'form_template_id' => 'required|exists:form_templates,id'
        ]);

        // Valiting
        if ($validator->fails()) {
            $json = [
                'message' => 'Error guardar',
                'errors' => $validator->errors()
            ];

            return response()->json($json);
        }

        $item = Form::where('tasks_visit_point_id', $visitPointId)->first();

        if (!isset($item)) {
            $item = new Form;
        }

        $item->fill($request->all());
        $item->tasks_visit_point_id = $visitPointId;
        $item->save();

        if (isset($request->formValues)) {
            $item->formValues()->delete();
            foreach ($request->formValues as $formValue) {
                $item->formValues()->save((new FormValue($formValue)));
            }
        }

        return response()->json($item);
    }

    public function getUser()
    {
        return auth('api')->user();
    }

}
