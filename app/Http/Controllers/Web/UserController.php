<?php

namespace IrisGPS\Http\Controllers\Web;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Requests\StoreUserRequest;

use IrisGPS\Http\Requests\UpdateOrDeleteUserRequest;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\User;

use IrisGPS\Privilege;

use Maatwebsite\Excel\Facades\Excel;

use Mail;

class UserController extends Controller
{

    public function __construct()
    {
        if(auth('admin')->check()) {

        }
        else {
            $this->middleware('verify-organization');
            $this->middleware('verify-privilege:view-user', ['only' => 'view']);
            $this->middleware('verify-privilege:create-user', ['only' => 'create']);
            $this->middleware('verify-privilege:store-user', ['only' => 'store']);
            $this->middleware('verify-privilege:edit-user', ['only' => ['edit', 'update']]);
            $this->middleware('verify-privilege:destroy-user', ['only' => 'destroy']);
            $this->middleware('owner-no-editable', ['only' => ['update','destroy']]);
        }
    }

    public function index(Request $request, $format = 'html')
    {
        $users = User::sameOrganization();
        $searchText = $request->search_text ? $request->search_text : null;
        $searchType = $request->search_type ? $request->search_type : null;
        
        $searchTypes = array(
            'email' => 'E-mail',
            'name' => 'Nombre',
        );

        if ($searchType && $searchText) {
            $users = $users->where(
                $searchType,
                'LIKE',
                '%' . $searchText . '%'
            );
        }

        $users = $users->paginate(20);
        $hasPrivilegesToCreate = auth()->user()->hasPrivilege('create-user');
        $hasPrivilegesToEdit = auth()->user()->hasPrivilege('edit-user');
        $hasPrivilegesToView = auth()->user()->hasPrivilege('view-user');
        $hasPrivilegesToDestroy = auth()->user()->isOwner();

        // Formats
        switch ($format) {
            case 'xls':
                Excel::create('Usuarios', function($excel) use ($users) {
                    $excel->sheet('Usuarios', function($sheet) use ($users) {
                        $sheet->row(1, ['id','Nombre', 'E-mail']);
                        $i = 2;
                        foreach ($users as $key => $value) {
                            $sheet->row($i, [
                                $value->id,
                                $value->name,
                                $value->email
                                ]);
                            $i++;
                        }
                    });

                })->download();

                break;
            case 'html':
                return view('web.users.index',compact(
                        'users',
                        'hasPrivilegesToCreate',
                        'hasPrivilegesToEdit',
                        'hasPrivilegesToDestroy',
                        'hasPrivilegesToView',
                        'searchTypes',
                        'searchType',
                        'searchText'
                    ));
                break;
        }
        
    }

    public function create()
    {
        return view('web.users.create');
    }

    public function store(StoreUserRequest $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->activated = 1;
        $user->save();

        $emailData = [
            'user' => $user->email,
            'password' => $request->password,
            'logoUrl' => 'http://sgventas.sgtel.pe/assets/irisgps/img-web/logo.png',
            'link' => 'http://sgventas.sgtel.pe/login'
        ];

        Mail::send('emails.welcome-to-user', $emailData, function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Datos de Acceso');
        });

        return response()
            ->redirectToAction('Web\UserController@index')
            ->withSuccess('Usuario creado exitosamente.');
    }

    public function view(User $user)
    {
        $hasPrivilegesToEdit = (
            (
                auth()->check() &&
                (auth()->user()->isOwner() || $user->id == auth()->user()->id)
            ) ||
            auth('admin')->check()
        );

        $data = compact(
            'user',
            'hasPrivilegesToEdit'
        );
        return view('web.users.view')->with($data);
    }

    public function edit(User $user)
    {
        $privileges = Privilege::all();
        return view('web.users.edit')->with(
            compact('user','hasPermissionsToEdit', 'privileges')
            );
    }

    public function update(Request $request, User $user)
    {
        $nextUrl = ($request->next_url ? $request->next_url : null);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = empty($request->password) ? $user->password : bcrypt($request->password);
        
        if ($request->has('privileges')) {
            $user->privileges()->sync($request->privileges);
        } else {
            $user->privileges()->sync([]);
        }

        $user->save();
        $response = response();

        if ($nextUrl) {
            $response = $response->redirectTo($nextUrl);
        } else {
            $response = $response->redirectToAction('Web\UserController@index');
        }

        $response = $response->withSuccess('Usuario actualizado exitosamente.');

        return $response;
    }

    public function destroy(User $user)
    {
        $user->delete();
        $message = "Usuario eliminado exitosamente.";
        return back()->withSuccess($message);
    }
}
