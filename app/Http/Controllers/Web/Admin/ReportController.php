<?php

namespace IrisGPS\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function index()
    {
    	return view('web.admin.reports.index');
    }

    public function a()
    {
    	return view('web.admin.reports.a');
    }

    public function b()
    {
    	return view('web.admin.reports.b');
    }

    public function c()
    {
    	return view('web.admin.reports.c');
    }
}
