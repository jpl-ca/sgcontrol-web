<?php

namespace IrisGPS\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Organization;

use IrisGPS\User;

use IrisGPS\Privilege;

class OrganizationUserController extends Controller
{
    public function index(Request $request, $organization_id, $format = 'html')
    {
        $organization = Organization::findOrFail($organization_id);
        $users = $organization->users();
        $searchText = $request->search_text ? $request->search_text : null;
        $searchType = $request->search_type ? $request->search_type : null;

        $searchTypes = array(
            'email' => 'E-mail',
            'name' => 'Nombre',
        );

        if ($searchType && $searchText) {
            $users = $users->where(
                $searchType,
                'LIKE',
                '%' . $searchText . '%'
            );
        }

        $users = $users->paginate(20);
        $hasPrivilegesToCreate = true;
        $hasPrivilegesToEdit = true;
        $hasPrivilegesToDestroy = true;

        // Formats
        switch ($format) {
            case 'xls':
                Excel::create('Usuarios', function($excel) use ($users) {
                    $excel->sheet('Usuarios', function($sheet) use ($users) {
                        $sheet->row(1, ['id','Nombre', 'E-mail']);
                        $i = 2;
                        foreach ($users as $key => $value) {
                            $sheet->row($i, [
                                $value->id,
                                $value->name,
                                $value->email
                                ]);
                            $i++;
                        }
                    });

                })->download();

                break;
            case 'html':
                return view('web.admin.organizations.users.index',compact(
                        'users',
                        'hasPrivilegesToCreate',
                        'hasPrivilegesToEdit',
                        'hasPrivilegesToDestroy',
                        'searchTypes',
                        'searchType',
                        'searchText',
                        'organization'
                    ));
                break;
        }
        
    }

    public function create()
    {
        return view('web.admin.users.create');
    }

    public function store(StoreUserRequest $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()
            ->redirectToAction('Web\Admin\UserController@index')
            ->withSuccess('Usuario creado exitosamente.');
    }

    public function view(Request $request,$organization_id, $user)
    {
        $hasPrivilegesToEdit = true;
        $organization = $user->organization;
        $data = compact(
            'user',
            'organization',
            'hasPrivilegesToEdit'
        );

        return view('web.admin.organizations.users.view')->with($data);
    }

    public function edit(Request $request, $organization_id, User $user)
    {
        $privileges = Privilege::all();
        $organization = $user->organization;

        return view('web.admin.organizations.users.edit')->with(
            compact('user','hasPermissionsToEdit', 'privileges', 'organization')
            );
    }

    public function update(Request $request, $organization_id, $user)
    {
        $nextUrl = ($request->next_url ? $request->next_url : null);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = empty($request->password) ? $user->password : bcrypt($request->password);

        if ($request->has('privileges')) {
            $user->privileges()->sync($request->privileges);
        }

        $user->save();
        $response = response();

        if ($nextUrl) {
            $response = $response->redirectTo($nextUrl);
        } else {
            $response = $response->redirectToAction('Web\Admin\OrganizationUserController@index', ['organization_id' => $user->organization_id, 'format' => 'html']);
        }

        $response = $response->withSuccess('Usuario actualizado exitosamente.');

        return $response;
    }

    public function destroy(User $user)
    {
        $user->delete();
        $message = "Usuario eliminado exitosamente.";
        return back()->withSuccess($message);
    }

    public function lock(Request $request, $organization_id, User $user)
    {
        $user->locked = true;
        $user->save();

        $response = response()
            ->redirectToAction('Web\Admin\OrganizationUserController@index',
                [
                    'organization_id' => $organization_id,
                    'format' => 'html'
                ])
            ->withSuccess('El usuario fue bloqueado');

        return $response;
    }

    public function unlock(Request $request, $organization_id, User $user)
    {
        $user->locked = false;
        $user->save();

        $response = response()
            ->redirectToAction('Web\Admin\OrganizationUserController@index',
                [
                    'organization_id' => $organization_id,
                    'format' => 'html'
                ])
            ->withSuccess('El usuario fue bloqueado');

        return $response;
    }
}
