<?php

namespace IrisGPS\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
    	if (!auth('admin')->check()) {
    		return response()->redirectTo('/admin/login');
    	}
    	return response()->redirectTo('/admin/organizations');
    }
}
