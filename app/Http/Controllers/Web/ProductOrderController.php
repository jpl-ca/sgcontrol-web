<?php

namespace IrisGPS\Http\Controllers\Web;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;

class ProductOrderController extends Controller
{
    public function index(Request $request)
    {
		return view('web.product-orders.index');
    }

    public function show(Request $request, $id)
    {
    	$data = [
    		'id' => $id
    	];

    	return view('web.product-orders.show', $data);
    }
}
