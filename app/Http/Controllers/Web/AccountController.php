<?php

namespace IrisGPS\Http\Controllers\Web;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Requests\StoreUserRequest;
use IrisGPS\Http\Requests\UpdateOrDeleteUserRequest;
use IrisGPS\Http\Controllers\Controller;
use IrisGPS\User;

class AccountController extends Controller
{
    public function index()
    {
    	$user = auth()->user();
        return view('web.account.index')->with(compact('user'));
    }

    public function edit()
    {
    	$user = auth('web')->user();
    	return view('web.account.edit')->with(compact('user'));
    }
}
