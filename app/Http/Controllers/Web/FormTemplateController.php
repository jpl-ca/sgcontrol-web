<?php

namespace IrisGPS\Http\Controllers\Web;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;

class FormTemplateController extends Controller
{
    public function index(Request $request)
    {
        return view('web.form-templates.index');
    }

    public function create(Request $request)
    {
        return view('web.form-templates.create');
    }

    public function show(Request $request, $id)
    {
        $data = ['id' => $id];
        return view('web.form-templates.show', $data);
    }

    public function edit(Request $request, $id)
    {
        $data = ['id' => $id];
        return view('web.form-templates.edit', $data);
    }
}
