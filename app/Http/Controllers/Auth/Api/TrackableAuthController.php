<?php

namespace IrisGPS\Http\Controllers\Auth\Api;

use Carbon\Carbon;
use IrisGPS\Agent;
use IrisGPS\ChecklistItem;
use IrisGPS\Http\Controllers\Controller;
use IrisGPS\Http\Requests;
use Illuminate\Http\Request;
use IrisGPS\Task;
use IrisGPS\TasksVisitPoint;
use IrisGPS\Vehicle;
use IrisGPS\VisitState;

/**
 * Class TrackableAuthController
 * @package IrisGPS\Http\Controllers\Api\Auth
 */
class TrackableAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => 'login']);
    }

    /**
     * Starts a session for a trackable object, creating and
     * returning a new api token by the given credentials.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $authentication_code = $request->get('authentication_code');
        $plate = $request->get('plate');
        $password = $request->get('password');

        if (!is_null($model = Agent::where('authentication_code', $authentication_code)->first())) {
            if (! password_verify($password, $model->password)) {
                $model = null;
            }
        } elseif (!is_null($model = Vehicle::where('plate', $plate)->first())) {
            if (! password_verify($password, $model->password)) {
                $model = null;
            }
        }

        if (!is_null($model)) {
                if (is_null($model->trackable->api_token)) {
                $model->trackable->regenerateToken();
                return response()->json($model->toArray());
            }
            return response('This device is already logged in.', 403);
        }

        return response('Invalid credentials.', 422);
    }

    /**
     * Closes current session by removing the api
     * token from active trackable object.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        if (auth('api')->check()) {
            auth('api')->user()->removeToken();
            return response(['data' => 'You have been logged out successfully.']);
        }
        return response('There are not any active session', 401);
    }

    /**
     * Returns the instance of the trackable object of the current session.
     *
     * @return \Illuminate\Http\Response
     */
    public function me()
    {
        if (auth('api')->check()) {
            return auth('api')->user()->trackable->toArray();
        }
        return response('There are not any active session', 401);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function updateNotificationId(Request $request)
    {
        $rules = [
            'notification_id' => 'required'
        ];
        $this->validate($request, $rules);
        auth('api')->user()->updateNotificationId($request->notification_id);
        return response(['data' => 'Notification id updated successfully.']);
    }

    public function storeGeolocation(Request $request)
    {
        $trackable = auth('api')->user();
        $trackableModel = $trackable->trackable;

        $rules = [
            'lat' => 'required|numeric|min:-90.999999999999999|max:90.999999999999999',
            'lng' => 'required|numeric|min:-180.999999999999999|max:180.999999999999999',
            'task_id' => 'can_be_done_by:' . $trackableModel->id . ',' . get_class($trackableModel),
        ];

        $this->validate($request, $rules);
        $geolocationHistory = [
            'lat' => (double) $request->lat,
            'lng' => (double) $request->lng,
            'task_id' => isset($request->task_id) ? $request->task_id : null
        ];
        $lastGeolocation = $trackable->lastGeolocation();

        if (isset($lastGeolocation) && $geolocationHistory['lat'] == $lastGeolocation->lat && $geolocationHistory['lng'] == $lastGeolocation->lng) {
            $lastGeolocation->created_at = Carbon::now();
            $lastGeolocation->updated_at = Carbon::now();
            $lastGeolocation->save();
            return response(['data' => 'Geolocation updated successfully.']);
        }

        $trackable->geolocationHistory()->create($geolocationHistory);
        return response(['data' => 'Geolocation recorded successfully.']);

    }

    public function taskList()
    {
        $trackable = auth('api')->user();
        $trackableModel = $trackable->trackable;
        $currentDate = Carbon::now();
        $endOfDay = $currentDate->copy()->endOfDay();

        $tasks = Task::where('taskable_id', $trackableModel->id)
            ->where('taskable_type', get_class($trackableModel))
            ->where('start_date', '<=', $currentDate->toDateTimeString())
            ->where('end_date', '>=', $currentDate->toDateTimeString())
            ->with('visitPoints', 'visitPoints.checklist')
            ->orderBy('start_date', 'asc')
            ->get()
            ->first();

        if (is_null($tasks)) {

            $tasks = Task::where('taskable_id', $trackableModel->id)
                ->where('taskable_type', get_class($trackableModel))
                ->where('start_date', '>=', $currentDate->toDateTimeString())
                ->where('end_date', '<=', $endOfDay->toDateTimeString())
                ->with('visitPoints', 'visitPoints.checklist')
                ->orderBy('start_date', 'asc')
                ->get()
                ->first();
        }

        return response($tasks);
    }

    public function checkListItem(Request $request)
    {
        $listItem = ChecklistItem::find($request->id);
        $listItem->checked = true;
        $listItem->save();

        return response(['data' => 'Item checked successfully.']);
    }

    public function uncheckListItem(Request $request)
    {
        $listItem = ChecklistItem::find($request->id);
        $listItem->checked = false;
        $listItem->save();

        return response(['data' => 'Item unchecked successfully.']);
    }

    public function rescheduleVisitPoint(Request $request)
    {
        $taskVisitPointId = $request->id;

        $taskVisitPoint = TasksVisitPoint::find($taskVisitPointId);

        if ($request->exists('comment_of_rescheduling')) {
            $taskVisitPoint->comment_of_rescheduling = $request->comment_of_rescheduling;
        }

        if ($taskVisitPoint->visit_state_id == VisitState::STATE_SCHEDULED || $taskVisitPoint->visit_state_id == VisitState::STATE_DONE ||
                ( $taskVisitPoint->productOrder &&
                    ($taskVisitPoint->productOrder->status == 'pending' || $taskVisitPoint->productOrder->status == 'canceled')
                )
            ) {
            $taskVisitPoint->visit_state_id = VisitState::STATE_RESCHEDULING_REQUEST;
            $taskVisitPoint->save();
            return response(['data' => 'Visit point state updated successfully.']);
        } else {
            if ($taskVisitPoint->visit_state_id == VisitState::STATE_RESCHEDULING_REQUEST) {
                return response(['data' => "Visit point can't be updated. Visit point's status is rescheduled"], 422);
            }

            if ($taskVisitPoint->visit_state_id == VisitState::STATE_CANCELLED) {
                return response(['data' => "Visit point can't be updated. Visit point's status was cancelled"], 422);
            }

            return response(['data' => "Visit point can't be updated"], 422);
        }

        
    }
}