<?php

namespace IrisGPS\Http\Middleware;

use Closure;
use Exception;

class VerifyPrivilege
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        if (is_null($role)) {
            throw new Exception('There is no role set on middleware!.');
        }else {
            if (auth()->user()->isOwner() || !auth()->user()->privileges()->where('privilege_id', $role)->get()->isEmpty()) {
                return $next($request);
            }
            return redirect()->action('Web\DashboardController@index')->withWarning('No tienes permisos suficientes para realizar esta acción. Contáctate con tu administrador. ');
        }
    }
}
