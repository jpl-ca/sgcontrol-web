<?php

namespace IrisGPS\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '*',
        'trackable-assets/destroy/*',
        'users/destroy/*',
        'markers/destroy/*',
        'tasks/destroy/*',
        'admin/organizations/*/destroy',
        'admin/organizations/*/lock',
        'admin/organizations/*/unlock',
        'admin/users/destroy/*',
        'admin/organizations/*/users/lock/*',
        'admin/organizations/*/users/unlock/*',
        'admin/organizations/*/payments/destroy/*',
        'admin/suggestions/destroy/*',
        'api/*'
    ];
}
