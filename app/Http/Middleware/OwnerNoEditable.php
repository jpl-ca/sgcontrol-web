<?php

namespace IrisGPS\Http\Middleware;

use Closure;
use Exception;

class OwnerNoEditable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $role
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $parametersName = $request->route()->parameterNames();

        if (isset($parametersName[0])) {
            $modelName = $parametersName[0];
            $user = $request->route()->parameters()[$modelName];
            
            if(!$user->isOwner() || auth()->user()->id == $user->id){
                return $next($request);
            }else{
                return redirect()->action('Web\UserController@index')->withWarning('You do not have enough privileges to edit Owner User!. ');
            }
        }else{
            throw new Exception('There is no user set on middleware!.');
        }
    }
}
