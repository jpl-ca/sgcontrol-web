<?php

namespace IrisGPS\Http\Middleware;

use Closure;

class VerifyOrganization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $parametersName = $request->route()->parameterNames();

        if (isset($parametersName) && in_array('userModel', $parametersName)) {
            $model = $request->route()->parameters()['userModel'];

            if ($model->organization_id !== auth()->user()->organization_id) {
                abort(403);
            }
        }

        return $next($request);
    }
}
