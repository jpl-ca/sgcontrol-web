<?php

namespace IrisGPS\Http\Requests;

use Carbon\Carbon;
use IrisGPS\Agent;
use IrisGPS\Http\Requests\Request;

class StoreVehicleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plate' => 'required|alpha_dash|min:4|max:255|unique:vehicles,plate',
            'brand' => 'required|min:4|max:255',
            'model' => 'required|min:4|max:255',
            'color' => 'required|min:3|max:255',
            'manufacture_year' => 'required|integer|between:1900,2016',
            'gas_consumption_rate' => 'required|numeric',
            'password' => 'required|alpha_num|max:255|min:4',
        ];
    }

    public function forbiddenResponse()
    {
        return response()->redirectToAction('WebDashboardController@dashboard');
    }
}
