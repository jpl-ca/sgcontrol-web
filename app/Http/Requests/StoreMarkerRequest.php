<?php

namespace IrisGPS\Http\Requests;

use IrisGPS\Http\Requests\Request;

class StoreMarkerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'address' => 'max:255',
            'phone' => 'digits_between:6,30',
            'reference' => 'max:255',
            'latitude' => 'required|numeric|min:-90.999999999999999|max:90.999999999999999',
            'longitude' => 'required|numeric|min:-180.999999999999999|max:180.999999999999999',
        ];
    }
}
