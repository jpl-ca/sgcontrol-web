<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class OrganizationPayment extends Model
{
    protected $fillable = [
    	'amount',
    	'organization_id',
    	'date',
    	'subscription_start_date',
    	'subscription_end_date',
    	'additional_information'
    ];

    protected $appends = [
    	'image_url'
    ];

    public function getImageUrlAttribute()
    {
    	return url("/admin/organizations/{$this->organization_id}/payments/view/{$this->id}/image");
    }
}
