<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    const TYPE_USER = 1;
    const TYPE_OWNER = 2;
    
    protected $appends = ['nameToSpanish'];

    public function getNameToSpanishAttribute()
    {
    	$val = $this->name;
    	$name = null;

    	if ($val == 'User') {
    		$name = 'Normal';
    	} elseif ($val == 'Owner') {
    		$name = 'Admin';
    	} else {
    		$name = 'Indefinido';
    	}

    	return $name;
    }
}
