<?php

namespace IrisGPS\Console\Commands;

use Illuminate\Console\Command;
use IrisGPS\Organization;
use Subscriber;

class ClearExpiredSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'irisgps:clear-expired-subscriptions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update current subscriber status.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $organizations = Organization::all();

        foreach ($organizations as $organization) {
            if ($organization->isCurrentPlanExpired()) {
                if ($organization->subscription_id == 'basic' || $organization->isGracePeriodExpired()) {
                    $basicPlan = Subscriber::plans()->find('basic');
                    Subscriber::subscribe($organization, $basicPlan);
                    $this->comment($organization->name . ' is renewed with a basic subscription');
                }
            }
        }
    }
}
