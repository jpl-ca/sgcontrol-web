<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class FormTemplateState
{
	public static $active = 'active';
	public static $inactive = 'inactive';
}
