<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }
}
