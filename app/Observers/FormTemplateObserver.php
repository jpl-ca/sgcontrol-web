<?php
namespace IrisGPS\Observers;

use IrisGPS\Organization;

use Log;

use IrisGPS\FormTemplateState;

class FormTemplateObserver
{
	public function creating($model)
	{

		if (!isset($model->state)) {
			$model->state = FormTemplateState::$inactive;
		}
		
	}
}
