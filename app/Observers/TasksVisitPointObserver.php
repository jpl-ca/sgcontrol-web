<?php

namespace IrisGPS\Observers;

use IrisGPS\TasksVisitPointHistory;

use IrisGPS\TasksVisitPoint;

use IrisGPS\VisitState;

use IrisGPS\Marker;

use Log;

use Carbon\Carbon;

use IrisGPS\Task;

class TasksVisitPointObserver
{
	public function creating($model)
	{
		if (!isset($model->datetime)) {
			$model->datetime = Carbon::now();
		}

		if (!isset($model->visit_state_id)) {
			$model->visit_state_id = VisitState::STATE_SCHEDULED;
		}
	}

	public function saved($model)
	{
		Log::info("TaskVisitPointObserver: SAVED");
		if ($model->isDirty()) {
			$dirty = $model->getDirty();
			Log::info("TaskVisitPointObserver: HAY CAMBIOS");
			Log::info($dirty);
			

			if (isset($dirty) && array_key_exists('visit_state_id', $dirty)) {
				Log::info("TaskVisitPointObserver: visit_state_id ha cambiado");
				$newTasksVisitPoint = new TasksVisitPointHistory;
				$newTasksVisitPoint->tasks_visit_point_id = $model->id;
				$newTasksVisitPoint->lat = $model->lat;
				$newTasksVisitPoint->lng = $model->lng;
				$newTasksVisitPoint->name = $model->name;
				$newTasksVisitPoint->address = $model->address;
				$newTasksVisitPoint->phone = $model->phone;
				$newTasksVisitPoint->reference = $model->reference;
				$newTasksVisitPoint->task_id = $model->task_id;
				$newTasksVisitPoint->visit_state_id = $model->visit_state_id;
				$newTasksVisitPoint->marker_id = $model->marker_id;
				$newTasksVisitPoint->visit_point_id_parent = $model->visit_point_id_parent;
				$newTasksVisitPoint->datetime = $model->datetime;
				$newTasksVisitPoint->comment_of_rescheduling = $model->comment_of_rescheduling;
				$newTasksVisitPoint->save();
			}

			if (isset($dirty) && array_key_exists('visit_point_id_parent', $dirty)) {
				Log::info('analizeChangesOnVisitPointAndChild');
				$this->analizeChangesOnVisitPointAndChild($model, $model->child);
			}

		}else{
			Log::info("'TasksVisitPoint': No ha cambiado");
		}

		if ($model->task_id) {
			$task = Task::find($model->task_id);
			$task->updateStartDateAndEndDateFromVisitPoints();
		}
	}

	public function deleted($model)
	{
		if ($model->task_id) $task = Task::find($model->task_id);
		if ($task) $task->deleteIfThisHasNotVisitPoints();
	}

	public function saving($model)
	{

		if ($model->isDirty()) {
			
			$dirty = $model->getDirty();

			if (isset($dirty['datetime'])) {

				$currentVisitStateId = (int)$model->visit_state_id;

				if ($currentVisitStateId == VisitState::STATE_RESCHEDULING_REQUEST) {
					$model->visit_state_id = VisitState::STATE_SCHEDULED;
				}
			}
		}

		if (isset($model->marker_id)) {
			Log::info("Saving task visit point :" . $model->marker_id);
			$marker = Marker::find($model->marker_id);
			$model->name = $marker->name;
			$model->address = $marker->address;
			$model->phone = $marker->phone;
			$model->reference = $marker->reference;
		} else {
			Log::info("Saving task visit point: No tiene task visit point");
		}
	}

	private function analizeChangesOnVisitPointAndChild($newVisitPoint, $oldVisitPoint)
	{
		$trackableMustReceiveNotification = false;

		$newVisitIsProcessable = ($newVisitPoint->task
								  && $newVisitPoint->task->taskable
								  && isset($newVisitPoint->datetime));

		$oldVisitIsProcessable = ($oldVisitPoint->task
								  && $oldVisitPoint->task->taskable
								  && isset($oldVisitPoint->datetime));


		if ($newVisitIsProcessable && $oldVisitIsProcessable) {

			$timeOfNewVisitPoint = Carbon::parse($newVisitPoint->datetime);
			$timeIsAllowed = $timeOfNewVisitPoint->isToday();

			$trackableMustReceiveNotification = (
					$newVisitPoint->task->taskable->trackable->id != $oldVisitPoint->task->taskable->trackable->id
				)
				&& isset($newVisitPoint->time)
				&& $timeIsAllowed;
		}

		if ($trackableMustReceiveNotification) {

			$data = [
				'typeOfEvent' => 'taskChanged',
				'taskId' => $newVisitPoint->task->id
			];

			Log::info($data);

			$newVisitPoint->task->taskable->trackable->sendPushNotification($data);

			Log::info('Sending push notification');
		}
	}
}