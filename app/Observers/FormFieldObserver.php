<?php
namespace IrisGPS\Observers;

use IrisGPS\Organization;

use Log;

class FormFieldObserver
{
	public function creating($model)
	{

		if (!isset($model->machine_name)) {
			$temp = strtolower($model->label);
			$model->machine_name = preg_replace('@[^a-z0-9_]+@','_', $temp);
		}
		
	}
}
