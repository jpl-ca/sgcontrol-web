<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class ChecklistItem extends Model
{
    public function taskVisitPoint()
    {
        return $this->belongsTo(TasksVisitPoint::class, 'tasks_visit_point_id', 'id');
    }
}
