<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;
use IrisGPS\Contracts\Trackable\TrackableContract;
use IrisGPS\Scopes\OrganizationScope;
use IrisGPS\Trackable\Trackable as TrackableTrait;

class Vehicle extends Model implements TrackableContract
{
    use TrackableTrait;

    public function __construct()
    {
        parent::__construct();
        static::addGlobalScope(new OrganizationScope);
        $this->initialize();
        $this->trackableType = 'vehicle';
    }
    protected $appends = ['full_name'];

    public function getFullNameAttribute()
    {
        return $this->plate;
    }
}
