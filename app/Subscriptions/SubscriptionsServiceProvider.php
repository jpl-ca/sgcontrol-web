<?php

namespace IrisGPS\Subscriptions;

use Illuminate\Support\ServiceProvider;
use Subscriber;

class SubscriptionsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->subscriptionPlans();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function subscriptionPlans()
    {
        Subscriber::free('Basic', 'basic')
            ->attributes([
                'duration_days' => 30,
                'trackable_limit' => 5,
                'tasks_per_trackable_day' => 5,
                'markers_limit' => 50,
                'markers_per_task' => 5,
                'querying_data_last_days' => 30
            ])
            ->features([
                'Up to 5 trackable devices',
                '1 task per trackable per day',
                '5 visit points per task',
                '50 markers',
                'Data storage for 30 days'
            ]);

        Subscriber::plan('Regular Monthly', 'regular-monthly')->price(100)
            ->attributes([
                'duration_days' => 30,
                'trackable_limit' => 10,
                'tasks_per_trackable_day' => null,
                'markers_limit' => null,
                'markers_per_task' => null,
                'querying_data_last_days' => null
            ])
            ->features([
                'Up to 10 trackable devices',
                'unlimited tasks per trackable per day',
                'unlimited visit points per task',
                'unlimited markers',
                'task rescheduling',
                'Data storage forever'
            ]);

        Subscriber::plan('Regular Yearly', 'regular-yearly')->price(1000)
            ->yearly()
            ->attributes([
                'duration_days' => 360,
                'trackable_limit' => 10,
                'tasks_per_trackable_day' => null,
                'markers_limit' => null,
                'markers_per_task' => null,
                'querying_data_last_days' => null
            ])
            ->features([
                'Up to 10 trackable devices',
                'unlimited tasks per trackable per day',
                'unlimited visit points per task',
                'unlimited markers',
                'task rescheduling',
                'Data storage forever'
            ]);
    }
}
