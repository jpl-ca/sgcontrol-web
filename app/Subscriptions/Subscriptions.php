<?php

namespace IrisGPS\Subscriptions;

use Carbon\Carbon;
use IrisGPS\Invoice;
use IrisGPS\Organization;

class Subscriptions
{
    protected static $plans;

    /**
     * Define a new free Subscription plan.
     *
     * @param  string  $name
     * @return \IrisGPS\Subscriptions\Plan
     */
    public static function free($name = 'Free', $id = null)
    {
        return static::plan($name, $id)->free();
    }
    /**
     * Define a new Subscription plan.
     *
     * @param  string  $name
     * @param  string  $id
     * @return \IrisGPS\Subscriptions\Plan
     */
    public static function plan($name, $id = null)
    {
        return static::plans()->create($name, $id);
    }
    /**
     * Get the Subscription plan collection.
     *
     * @return \IrisGPS\Subscriptions\Plans
     */
    public static function plans()
    {
        return static::$plans ?: static::$plans = new Plans;
    }

    public static function subscribe(Organization $organization, Plan $plan)
    {
        $organization->subscription_id = $plan->id;
        $organization->subscription_ends = Carbon::now()->endOfDay()->addDays($plan->attributes['duration_days'])->toDateTimeString();
        $organization->save();
        $detail = 'Charged ' . $plan->currencySymbol . $plan->price . ' for a ' . ($plan->isMonthly() ? 'Monthly' : 'Yearly') . ' subscription to ' . $plan->name . ' plan.';
        $invoice = new Invoice;
        $invoice->detail = $detail;
        $invoice->amount =  $plan->price;
        $organization->invoices()->save($invoice);
    }
}