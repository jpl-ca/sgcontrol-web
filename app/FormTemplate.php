<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class FormTemplate extends Model
{
    protected $fillable = [
        'title',
        'description',
        'state',
        'organization_id'
    ];

    public function formFields()
    {
    	return $this->hasMany(FormField::class);
    }

    public static function getCurrentActive()
    {
        return FormTemplate::where('state', 'active')
            ->orderBy('created_at', 'desc')
            ->first();
    }
}
