<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class FormField extends Model
{
    protected $fillable = [
    	'label',
    	'type',
    	'description',
    	'form_template_id'
    ];
}
