<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class VisitState extends Model
{
    const STATE_SCHEDULED = 1;
    const STATE_DONE = 2;
    const STATE_RESCHEDULING_REQUEST = 3;
    const STATE_CANCELLED = 4;

    protected $appends = ['name_to_spanish'];

    public function getNametoSpanishAttribute()
    {
    	$val = null;
    	switch ($this->id) {
    		case self::STATE_SCHEDULED :
    			$val = 'programado';
    			break;
    		case self::STATE_DONE :
    			$val = 'terminado';
    			break;
    		case self::STATE_RESCHEDULING_REQUEST :
    			$val = 'reprogramado';
    			break;
    		case self::STATE_CANCELLED :
    			$val = 'cancelado';
    			break;
    		default:
    			$val = 'indefinido';
    			break;
    	}

    	return $val;
    }
}
