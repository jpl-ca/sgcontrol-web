<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
	protected $fillable = [
		'title',
		'description',
		'tasks_visit_point_id',
		'form_template_id'
	];

    public function formValues() 
    {
		return $this->hasMany(FormValue::class);
    }
}
