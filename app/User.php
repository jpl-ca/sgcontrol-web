<?php

namespace IrisGPS;

use Illuminate\Foundation\Auth\User as Authenticatable;
use IrisGPS\Scopes\OrganizationScope;
use IrisGPS\Privilege;
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'organization_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeSameOrganization($query)
    {
        if (auth('web')->check()) {
            return $query->where('organization_id', auth('web')->user()->organization_id);
        }
        return $query;
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(UserType::class, 'user_type_id', 'id');
    }

    public function privileges()
    {
        return $this->belongsToMany(Privilege::class, 'user_privileges', 'user_id', 'privilege_id');
    }

    public function isOwner()
    {
        return $this->user_type_id == UserType::TYPE_OWNER;
    }

    public function isUser()
    {
        return $this->user_type_id == UserType::TYPE_USER;
    }

    public function hasPrivilege($privilege_name)
    {
        if (is_string($privilege_name)) {
            return ($this->privileges()->where('privilege_id', $privilege_name)->count() > 0);
        }

        if (is_array($privilege_name)) {
            return ($this->privileges()->whereIn('privilege_id', $privilege_name)->count() > 0);
        }

        return false;
    }
}
