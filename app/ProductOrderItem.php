<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class ProductOrderItem extends Model
{
    protected $fillable = [
    	'name',
    	'price',
    	'product_id',
    	'product_order_id',
    	'quantity',
    	'description'
    ];

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}
