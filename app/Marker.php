<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;
use IrisGPS\Scopes\OrganizationScope;

class Marker extends Model
{
    public function __construct()
    {
        parent::__construct();
        static::addGlobalScope(new OrganizationScope);
    }
}
