<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;
use IrisGPS\Scopes\OrganizationScope;
use IrisGPS\TasksVisitPoint;
use IrisGPS\ChecklistItem;

use Carbon\Carbon;

use Log;

class Task extends Model
{
    public function __construct()
    {
        parent::__construct();
        static::addGlobalScope(new OrganizationScope);
    }

    /**
     * Get all of the owning likeable models.
     */
    public function taskable()
    {
        return $this->morphTo();
    }

    public function visitPoints()
    {
        return $this->hasMany(TasksVisitPoint::class, 'task_id', 'id');
    }

    public function geolocationHistory()
    {
        return $this->hasMany(GeolocationHistory::class, 'task_id', 'id');
    }

    public function hasGeolocation()
    {
        return $this->geolocationHistory()
                ->get()->count() > 0;
    }

    public function lastGeolocation()
    {
        return $this->geolocationHistory()->orderBy('created_at', 'desc')->first();
    }

    public function isDateFinished()
    {
        return $this->end_date->isPast();
    }

    public function getTrackableInfoWindow()
    {
        if ($this->taskable->trackable->isAgent()) {
            $view = view('web.trackables.agents.partials.info-window')->with('agent', $this->taskable)->render();
        } elseif ($this->taskable->trackable->isVehicle()) {
            $view = view('web.trackables.vehicles.partials.info-window')->with('vehicle', $this->taskable)->render();
        }

        return $view;
    }

    public function saveVisitPoints($visitPoints)
    {
        if (is_array($visitPoints) && isset($visitPoints)) {
            
            foreach ($visitPoints as $visitPoint) {

                if (isset($visitPoint["id"])) {
                    $visitPointId = $visitPoint["id"];
                }

                if (isset($visitPoint['key'])) {
                    $visitPointId = $visitPoint["key"];
                }

                if (isset($visitPointId)) {
                    $newVisitPoint = TasksVisitPoint::find($visitPointId);
                }

                if (!isset($newVisitPoint)) {
                    $newVisitPoint = new TasksVisitPoint;
                }

                if (isset($visitPoint["lat"])) $newVisitPoint->lat = $visitPoint["lat"];
                if (isset($visitPoint["lng"])) $newVisitPoint->lng = $visitPoint["lng"];
                if (isset($visitPoint["name"])) $newVisitPoint->name = $visitPoint["name"];
                if (isset($visitPoint["address"])) $newVisitPoint->address = $visitPoint["address"];
                if (isset($visitPoint["phone"])) $newVisitPoint->phone = $visitPoint["phone"];
                if (isset($visitPoint["reference"])) $newVisitPoint->reference = $visitPoint["reference"];
                if (isset($visitPoint["marker_id"])) $newVisitPoint->marker_id = $visitPoint["marker_id"];
                if (isset($visitPoint["datetime"])) $newVisitPoint->datetime = $visitPoint["datetime"];
                if (isset($visitPoint["visit_point_id_parent"])) $newVisitPoint->marker_id = $visitPoint["visit_point_id_parent"];

                $newVisitPoint->task_id = $this->id;
                $newVisitPoint->save();

                $existsChecklist = (isset($visitPoint['checklist']) && is_array($visitPoint['checklist']));

                if ($existsChecklist) {

                    $checklist = [];
                    $newVisitPoint->checklist()->delete();

                    foreach ($visitPoint['checklist'] as $list_it) {
                        $checkListItem = new ChecklistItem;
                        $checkListItem->description = $list_it['description'];
                        array_push($checklist, $checkListItem);
                    }

                    $checklistItems = $newVisitPoint->checklist()->saveMany($checklist);    
                }

            }
        }
    }

    public function updateStartDateAndEndDateFromVisitPoints()
    {
        $newStartDate = null;
        $newEndDate = null;
        $visitPoints = TasksVisitPoint::where('task_id', $this->id);
        if($visitPoints->count() > 0) {
            $newStartDate = $visitPoints->orderBy('datetime', 'asc')->first()->datetime;
            $newEndDate = $visitPoints->orderBy('datetime', 'desc')->first()->datetime;

            if ($newStartDate) {
                $newStartDate = Carbon::parse(Carbon::parse($newStartDate)->toDateString());
            }

            if ($newEndDate) {
                $newEndDate = Carbon::parse(Carbon::parse($newEndDate)->toDateString())->addHours(24)->addSeconds(-1);
            }

        } else {
            Log::info("No pasaaa" . $this->id);
        }

        Log::info("Updating start_date = " . $newStartDate . " and end_date " . $newEndDate);

        if ($newStartDate || $newEndDate) {
            if ($newStartDate) $this->start_date = $newStartDate;
            
            $this->end_date = $newEndDate;

            $this->save();
        }
    }

    public function deleteIfThisHasNotVisitPoints()
    {
        if ($this->visitPoints()->count() == 0 ) {
            $this->delete();
        }
    }
}
