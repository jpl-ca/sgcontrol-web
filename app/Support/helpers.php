<?php

if (! function_exists('carbonFormat')) {
    function carbonFormat($date, $format = 'Y-m-d')
    {
        $date = new Carbon\Carbon($date);
        return $date->format($format);
    }
}

if (! function_exists('dateAsCarbon')) {
    function dateAsCarbon($date, $format = 'Y-m-d H:i:s')
    {
        $date = \Carbon\Carbon::createFromFormat($format, $date);
        return $date;
    }
}

function distanceBetweenPoints($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
      return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
  } else {
      return $miles;
  }
}